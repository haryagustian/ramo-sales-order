CREATE SCHEMA IF NOT EXISTS sales_order.hary;
CREATE TABLE IF NOT EXISTS sales_order.customer;
CREATE TABLE IF NOT EXISTS sales_order.product;
CREATE TABLE IF NOT EXISTS sales_order.sales;
CREATE TABLE IF NOT EXISTS sales_order.sales_detail;