package com.hary.agustian.services;

import com.barrans.util.*;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.hary.agustian.models.CustomerModel;
import io.quarkus.narayana.jta.runtime.TransactionConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Parameter;
import javax.persistence.Query;
import javax.transaction.Transactional;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@ApplicationScoped
public class CustomerService implements IAction {

  private static final Logger LOGGER = LoggerFactory.getLogger(CustomerService.class.getName());

  @Inject
  EntityManager em;

  @Override
  @Transactional
  @TransactionConfiguration(timeout = 30)
  public SimpleResponse insert(Object param, String header) {
    try {

      ObjectMapper om = new ObjectMapper();

      Map<String, Object> reqHeader = om.readValue(header, Map.class);

      if (reqHeader.get("userId") == null || reqHeader.get("userId").toString().equals(GeneralConstants.EMPTY_STRING)){
        return new SimpleResponse(GeneralConstants.VALIDATION_CODE, GeneralConstants.UNAUTHORIZED, new HashMap<>());
      }

      CustomerModel customer = om.convertValue(param, CustomerModel.class);

      if (customer.name == null || customer.name.equals(GeneralConstants.EMPTY_STRING)){
        return new SimpleResponse(GeneralConstants.VALIDATION_CODE, "customer name is required", new HashMap<>());
      }

      if (customer.phone == null || customer.phone.equals(GeneralConstants.EMPTY_STRING)){
        return new SimpleResponse(GeneralConstants.VALIDATION_CODE, "phone number is required", new HashMap<>());
      }

      if (customer.address == null || customer.address.equals(GeneralConstants.EMPTY_STRING)){
        return new SimpleResponse(GeneralConstants.VALIDATION_CODE, "address is required", new HashMap<>());
      }

      ObjectActiveAndCreatedDateUtil.registerObject(customer, reqHeader.get("userId").toString());
      customer.persist();


      return new SimpleResponse(GeneralConstants.SUCCESS_CODE, GeneralConstants.SUCCESS, getCustomer(customer.id));

    }catch (Exception event){
      LOGGER.error(event.getMessage(), event);
      return new SimpleResponse(GeneralConstants.FAIL_CODE, GeneralConstants.FAILED, new HashMap<>());
    }
  }

  @Override
  @Transactional
  @TransactionConfiguration(timeout = 30)
  public SimpleResponse update(Object param, String header) {
    try {

      ObjectMapper om = new ObjectMapper();

      Map<String, Object> reqHeader = om.readValue(header, Map.class);

      if (reqHeader.get("userId") == null || reqHeader.get("userId").toString().equals(GeneralConstants.EMPTY_STRING)){
        return new SimpleResponse(GeneralConstants.VALIDATION_CODE, GeneralConstants.UNAUTHORIZED, new HashMap<>());
      }

      Map<String, Object> reqParam = om.convertValue(param, Map.class);

      if (reqParam.get("id") == null || reqParam.get("id").toString().equals(GeneralConstants.EMPTY_STRING)){
        return new SimpleResponse(GeneralConstants.VALIDATION_CODE, "id is required", new HashMap<>());
      }

      CustomerModel customer = CustomerModel.findById(reqParam.get("id").toString());

      if (customer == null){
        return new SimpleResponse(GeneralConstants.VALIDATION_CODE, "customer does not exists", new HashMap<>());
      }

      if (reqParam.get("name") != null){
        customer.name = reqParam.get("name").toString();
      }

      if (reqParam.get("phone") != null){
        customer.phone =  reqParam.get("phone").toString();
      }

      if (reqParam.get("address") != null){
        customer.address = reqParam.get("address").toString();
      }

      ObjectActiveAndCreatedDateUtil.updateObject(customer, reqHeader.get("userId").toString(), customer.isActive);
      customer.persist();

      return new SimpleResponse(GeneralConstants.SUCCESS_CODE, GeneralConstants.SUCCESS, getCustomer(customer.id));

    }catch (Exception event){
      LOGGER.error(event.getMessage(), event);
      return new SimpleResponse(GeneralConstants.FAIL_CODE, GeneralConstants.FAILED, new HashMap<>());
    }
  }

  @Override
  public SimpleResponse inquiry(Object param) {
    try {

      ObjectMapper om = new ObjectMapper();
      Map<String, Object> request = om.convertValue(param, new TypeReference<>(){});

      Boolean filterPhone = false, filterName = false,
          filterAddress = false;

      Integer limit = 5, offset = 0;


      StringBuilder queryString = new StringBuilder();
      queryString.append("select id, name, phone, address from hary.customer where true ");


      if (request.containsKey("limit") && null != request.get("limit"))
        limit = Integer.parseInt(request.get("limit").toString());

      if (request.containsKey("offset") && null != request.get("offset"))
        offset = Integer.parseInt(request.get("offset").toString());

      if(request.containsKey("name") && request.get("name") != null) {
        queryString.append(" and \"name\" ilike :paramName ");
        filterName = true;
      }

      if(request.containsKey("phone") && request.get("phone") != null) {
        queryString.append(" and phone = :paramPhone ");
        filterPhone = true;
      }

      if(request.containsKey("address") && request.get("address") != null) {
        queryString.append(" and address ilike :paramAddress ");
        filterAddress = true;
      }

      queryString.append(" order by id desc");

      Query query = em.createNativeQuery(queryString.toString());

      if (filterName)
        query.setParameter("paramName", "%" + request.get("name").toString() + "%");

      if (filterPhone)
        query.setParameter("paramPhone", request.get("phone").toString());

      if (filterAddress)
        query.setParameter("paramAddress", "%" + request.get("address").toString() + "%");

      if (!limit.equals(-99) || !offset.equals(-99) || limit >= 0 || offset >= 0) {
        query.setFirstResult(offset);
        query.setMaxResults(limit);
      }

      List<Object[]> result = query.getResultList();
      List<Map<String,Object>> data =
          BasicUtils.createListOfMapFromArray(
              result,
              "id",
              "name",
              "phone",
              "address"
          );

      Query qCount = em.createNativeQuery(String.format(queryString.toString()
          .replaceFirst("select.* from", "select count(*) from ").replaceFirst("order by.*", "")));

      for (Parameter parameter : query.getParameters())
        qCount.setParameter(parameter.getName(), query.getParameterValue(parameter));

      BigInteger count = (BigInteger) qCount.getSingleResult();

      Map<String,Object> response = new HashMap<>();
      response.put("data", data);
      response.put("total", count);
      response.put("filtered", data.size());

      return new SimpleResponse(GeneralConstants.SUCCESS_CODE, GeneralConstants.SUCCESS, response);
    }catch (Exception event){
      LOGGER.error(event.getMessage(), event);
      return new SimpleResponse(GeneralConstants.FAIL_CODE, GeneralConstants.FAILED, new String());
    }
  }

  @Override
  public SimpleResponse entity(Object param) {
    try {

      ObjectMapper om = new ObjectMapper();

      Map<String, Object> reqParam = om.convertValue(param, new TypeReference<>(){});

      if (reqParam.get("id") == null || reqParam.get("id").toString().equals(GeneralConstants.EMPTY_STRING)){
        return new SimpleResponse(GeneralConstants.VALIDATION_CODE, "id is required", new HashMap<>());
      }

      CustomerModel response = CustomerModel.findById(reqParam.get("id").toString());

      if (response == null)
        return new SimpleResponse(GeneralConstants.VALIDATION_CODE, "customer does not exists", new HashMap<>());
      else
        return new SimpleResponse(GeneralConstants.SUCCESS_CODE, GeneralConstants.SUCCESS, getCustomer(response.id));

    }catch (Exception event){
      LOGGER.error(event.getMessage(), event);
      return new SimpleResponse(GeneralConstants.FAIL_CODE, GeneralConstants.FAILED, new HashMap<>());
    }
  }

  @Transactional
  @TransactionConfiguration(timeout = 20)
  public SimpleResponse postInsert(Object param, String header){
    try {

      ObjectMapper om = new ObjectMapper();

      Map<String, Object> reqHeader = om.readValue(header, Map.class);

      if (reqHeader.get("userId") == null || reqHeader.get("userId").toString().equals(GeneralConstants.EMPTY_STRING)){
        return new SimpleResponse(GeneralConstants.VALIDATION_CODE,  GeneralConstants.UNAUTHORIZED, new String());
      }

      CustomerModel customer = om.convertValue(param, CustomerModel.class);

      if (customer.name == null || customer.name.equals(GeneralConstants.EMPTY_STRING)){
        return new SimpleResponse(GeneralConstants.VALIDATION_CODE, "name is required", new String());
      }

      if (customer.phone == null || customer.phone.equals(GeneralConstants.EMPTY_STRING)){
        return new SimpleResponse(GeneralConstants.VALIDATION_CODE, "phone is required", new String());
      }

      if (customer.address == null || customer.address.equals(GeneralConstants.EMPTY_STRING)){
        return new SimpleResponse(GeneralConstants.VALIDATION_CODE, "address is required", new String());
      }

      Query result = insertCustomer(customer.name, customer.phone, customer.address, reqHeader.get("userId").toString(), reqHeader.get("userId").toString());

      result.executeUpdate();

      Object[] response = findBy(customer.name, customer.phone, customer.address, reqHeader.get("userId").toString(), reqHeader.get("userId").toString());

      return new SimpleResponse(GeneralConstants.SUCCESS_CODE, GeneralConstants.SUCCESS, getCustomer(response[0].toString()));

    }catch (Exception event){
      LOGGER.error(event.getMessage(), event);
      return new SimpleResponse(GeneralConstants.FAIL_CODE, event.getMessage(), event);
    }
  }

  @Transactional
  @TransactionConfiguration(timeout = 20)
  public SimpleResponse postUpdate(Object param, String header){
    try {

      ObjectMapper om = new ObjectMapper();

      Map<String, Object> reqHeader = om.readValue(header, Map.class);

      if (reqHeader.get("userId") == null || reqHeader.get("userId").toString().equals(GeneralConstants.EMPTY_STRING)){
        return new SimpleResponse(GeneralConstants.VALIDATION_CODE,  GeneralConstants.UNAUTHORIZED, new String());
      }

      Map<String, Object> reqParam = om.convertValue(param, Map.class);

      if (reqParam.get("id") == null || reqParam.get("id").toString().equals(GeneralConstants.EMPTY_STRING))
        return new SimpleResponse(GeneralConstants.VALIDATION_CODE, "id is required", new String());

      CustomerModel customer = CustomerModel.findById(reqParam.get("id").toString());

      if (customer == null)
        return new SimpleResponse(GeneralConstants.VALIDATION_CODE, "customer does not exist", new String());

      Query result = updateCustomer(reqParam.get("id").toString(), reqParam.get("name").toString(), reqParam.get("phone").toString(), reqParam.get("address").toString(), reqHeader.get("userId").toString());

      result.executeUpdate();

      return new SimpleResponse(GeneralConstants.SUCCESS_CODE, GeneralConstants.SUCCESS, getCustomer(reqParam.get("id").toString()));

    }catch (Exception event){
      LOGGER.error(event.getMessage(), event);
      return new SimpleResponse(GeneralConstants.FAIL_CODE, event.getMessage(), event);
    }
  }

  @Transactional
  public SimpleResponse postDelete(Object param, String header){
    try {

      ObjectMapper om = new ObjectMapper();

      Map<String, Object> reqHeader = om.readValue(header, Map.class);

      if (reqHeader.get("userId") == null || reqHeader.get("userId").toString().equals(GeneralConstants.EMPTY_STRING))
        return new SimpleResponse(GeneralConstants.VALIDATION_CODE,  GeneralConstants.UNAUTHORIZED, new String());


      Map<String, Object> reqParam = om.convertValue(param, Map.class);

      if (reqParam.get("id") == null || reqParam.get("id").toString().equals(GeneralConstants.EMPTY_STRING))
        return new SimpleResponse(GeneralConstants.VALIDATION_CODE, "id is required", new String());

      CustomerModel customer = CustomerModel.findById(reqParam.get("id").toString());


      if (customer == null){
        return new SimpleResponse(GeneralConstants.VALIDATION_CODE, "customer does not exists", GeneralConstants.EMPTY_STRING);
      }

      deleteCustomer(reqParam.get("id").toString()).executeUpdate();

      return new SimpleResponse(GeneralConstants.SUCCESS_CODE, "customer has been deleted", GeneralConstants.EMPTY_STRING);

    }catch (Exception event){
      LOGGER.error(event.getMessage(), event);
      return new SimpleResponse(GeneralConstants.FAIL_CODE, event.getMessage(), event);
    }
  }

  public SimpleResponse getListCustomer(){
    try {

      StringBuilder sb = new StringBuilder();

      sb.append("select c.id , c.\"name\" , c.phone , c.address");
      sb.append(" from hary.customer c");
      sb.append(" order by c.id desc");

      Query query = em.createNativeQuery(sb.toString());

      List<Map<String, Object>> response = BasicUtils.createListOfMapFromArray(query.getResultList(), "id", "name", "description", "price");

      return new SimpleResponse(GeneralConstants.SUCCESS_CODE, GeneralConstants.SUCCESS, response);

    }catch (Exception event){
      LOGGER.error(event.getMessage(), event);
      return new SimpleResponse(GeneralConstants.FAIL_CODE, GeneralConstants.FAILED, event.getMessage() + event);
    }
  }

  public Object[] getVersionCustomer(String id){

    StringBuilder sb = new StringBuilder();
    sb.append("select id, name, phone, address, version from hary.customer where id = :id");

    Query query = em.createNativeQuery(sb.toString());
    query.setParameter("id", id);

    return (Object[]) query.getSingleResult();

  }

  public Object[] findBy(String name, String phone, String address, String createdBy, String updateBy){
    Query findQuery = em.createNativeQuery("select id, name, phone, address from hary.customer where name = :paramName and phone = :paramPhone and address = :paramAddress and created_by = :paramCreated and updated_by = :paramUpdated");
    findQuery.setParameter("paramName", name);
    findQuery.setParameter("paramPhone", phone);
    findQuery.setParameter("paramAddress", address);
    findQuery.setParameter("paramCreated", createdBy);
    findQuery.setParameter("paramUpdated", updateBy);

    return (Object[]) findQuery.getSingleResult();
  }

  public Query insertCustomer(String name, String phone, String address, String createdBy, String updateBy){
    StringBuilder sb = new StringBuilder();
    sb.append("insert into hary.customer");
    sb.append(" (id, active_at, created_at, created_by, inactive_at, is_active, updated_at, updated_by, \"version\", address, name, phone) values");
    sb.append(" (gen_random_uuid(), pg_catalog.now(), pg_catalog.now(), :createdBy, null, true, pg_catalog.now(), :updatedBy, 0, :address, :name, :phone)");

    Query query = em.createNativeQuery(sb.toString());
    query.setParameter("name", name);
    query.setParameter("phone", phone);
    query.setParameter("address", address);
    query.setParameter("createdBy", createdBy);
    query.setParameter("updatedBy", updateBy);

    return query;
  }

  public Query updateCustomer(String id, String name, String phone, String address, String updatedBy){
    StringBuilder sb = new StringBuilder();
    sb.append("UPDATE hary.customer");
    sb.append(" SET updated_at = pg_catalog.now(), updated_by = :paramUpdated, \"version\"= :paramVersion, address = :paramAddress, \"name\" = :paramName, phone = :paramPhone");
    sb.append(" where id = :id");

    Query query = em.createNativeQuery(sb.toString());

    if (id != null)
      query.setParameter("id", id);

    if (name != null)
      query.setParameter("paramName", name);

    if (phone != null)
      query.setParameter("paramPhone", phone);

    if (address != null)
      query.setParameter("paramAddress", address);

    if (updatedBy != null)
      query.setParameter("paramUpdated", updatedBy);

    Object[] versionCustomer = getVersionCustomer(id);

    query.setParameter("paramVersion", Long.parseLong(versionCustomer[4].toString()) + 1L);

    return query;
  }

  public Query deleteCustomer(String id){
    Query query = em.createNativeQuery("delete from hary.customer where id = :id");

    if (id != null)
      query.setParameter("id", id);

    return query;
  }

  public Map<String, Object> getCustomer(String id) throws Exception {
    StringBuilder sb = new StringBuilder();
    sb.append("select id, name, phone, address");
    sb.append(" from hary.customer");
    sb.append(" where id = :id");

    Query query = em.createNativeQuery(sb.toString());

    query.setParameter("id", id);

    Object[] result = (Object[]) query.getSingleResult();


    Map<String, Object> customer = BasicUtils
        .createMapFromArray(result, "id",
            "name",
            "phone",
            "address");



    return customer;
  }
}
