package com.hary.agustian.services;

import com.barrans.util.*;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.hary.agustian.models.*;
import io.quarkus.narayana.jta.runtime.TransactionConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Parameter;
import javax.persistence.Query;
import javax.transaction.Transactional;
import java.math.BigInteger;
import java.text.ParseException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@ApplicationScoped
public class PurchaseDetailService implements IAction {

  private static final Logger LOGGER = LoggerFactory.getLogger(PurchaseDetailService.class.getName());

  @Inject
  EntityManager em;

  @Inject
  PurchaseService purchaseService;

  @Inject
  ProductService productService;

  @Override
  @Transactional
  @TransactionConfiguration(timeout = 30)
  public SimpleResponse insert(Object param, String header) {
    try {

      ObjectMapper om = new ObjectMapper();

      Map<String, Object> reqHeader = om.readValue(header, Map.class);

      if (reqHeader.get("userId") == null || reqHeader.get("userId").toString().equals(GeneralConstants.EMPTY_STRING)){
        return new SimpleResponse(GeneralConstants.VALIDATION_CODE, GeneralConstants.UNAUTHORIZED, new HashMap<>());
      }

      PurchaseDetailModel pd = om.convertValue(param, PurchaseDetailModel.class);

      if (pd.quantity == null){
        return new SimpleResponse(GeneralConstants.VALIDATION_CODE, "quantity is required", new HashMap<>());
      }


      ObjectActiveAndCreatedDateUtil.registerObject(pd, reqHeader.get("userId").toString());
      pd.persist();

      return new SimpleResponse(GeneralConstants.SUCCESS_CODE, GeneralConstants.SUCCESS, getPurchaseDetail(pd.id));

    }catch (Exception event){
      LOGGER.error(event.getMessage(), event);
      return new SimpleResponse(GeneralConstants.FAIL_CODE, GeneralConstants.FAILED, event.getMessage() + event);
    }
  }

  @Override
  @Transactional
  @TransactionConfiguration(timeout = 30)
  public SimpleResponse update(Object param, String header) {
    try {

      ObjectMapper om = new ObjectMapper();

      Map<String, Object> reqHeader = om.readValue(header, Map.class);

      if (reqHeader.get("userId") == null || reqHeader.get("userId").toString().equals(GeneralConstants.EMPTY_STRING)){
        return new SimpleResponse(GeneralConstants.VALIDATION_CODE, GeneralConstants.UNAUTHORIZED, new HashMap<>());
      }

      Map<String, Object> reqParam = om.convertValue(param, Map.class);

      if (reqParam.get("id") == null || reqParam.get("id").toString().equals(GeneralConstants.EMPTY_STRING)){
        return new SimpleResponse(GeneralConstants.VALIDATION_CODE, GeneralConstants.UNAUTHORIZED, new HashMap<>());
      }

      PurchaseDetailModel pd = PurchaseDetailModel.findById(reqParam.get("id").toString());

      if (pd == null){
        return new SimpleResponse(GeneralConstants.VALIDATION_CODE, "purchase detail does not exists", new HashMap<>());
      }

      if (pd.quantity != null){
        pd.quantity = Integer.parseInt(reqParam.get("quantity").toString());
      }

      ObjectActiveAndCreatedDateUtil.updateObject(pd, reqHeader.get("userId").toString());
      pd.persist();

      return new SimpleResponse(GeneralConstants.SUCCESS_CODE, GeneralConstants.SUCCESS, getPurchaseDetail(pd.id));

    }catch (Exception event){
      LOGGER.error(event.getMessage(), event);
      return new SimpleResponse(GeneralConstants.FAIL_CODE, GeneralConstants.FAILED, event + event.getMessage());
    }
  }

  @Override
  public SimpleResponse inquiry(Object param) {
    try {

      ObjectMapper om = new ObjectMapper();

      Map<String, Object> request = om.convertValue(param, new TypeReference<>(){});

      Integer limit = 5, offset = 0;

      Boolean filterQuantity = false;

      if (request.containsKey("limit") && request.get("limit") != null){
        limit = Integer.parseInt(request.get("limit").toString());
      }

      if (request.containsKey("offset") && request.get("offset") != null){
        offset = Integer.parseInt(request.get("offset").toString());
      }

      StringBuilder queryString = new StringBuilder();
      queryString.append("select id, quantity from hary.purchase_detail where true");

      if (request.containsKey("quantity") && request.get("quantity") != null){
        queryString.append(" and quantity = :paramQuantity ");
        filterQuantity = true;
      }

      queryString.append("order by id desc");

      Query query = em.createNativeQuery(queryString.toString());

      if (filterQuantity){
        query.setParameter("paramQuantity", Integer.parseInt(request.get("quantity").toString()));
      }

      if (!limit.equals(-99) || !offset.equals(-99) || limit >= 0 || offset >=0){
        query.setMaxResults(limit);
        query.setFirstResult(offset);
      }

      List<Object[]> result = query.getResultList();

      List<Map<String, Object>> data = BasicUtils.createListOfMapFromArray(result, "id", "quantity");

      Query qCount = em.createNativeQuery(String.format(queryString.toString().replaceFirst("select.* from", "select count (*) from").replaceFirst("order by.*", "")));

      for (Parameter queryParam : query.getParameters()){
        qCount.setParameter(queryParam.getName(), query.getParameterValue(queryParam));
      }

      BigInteger count = (BigInteger) qCount.getSingleResult();

      Map<String, Object> response = new HashMap<>();
      response.put("data", data);
      response.put("total", count);
      response.put("filtered", data.size());

      return new SimpleResponse(GeneralConstants.SUCCESS_CODE, GeneralConstants.SUCCESS, response);

    }catch (Exception event){
      LOGGER.error(event.getMessage(), event);
      return new SimpleResponse(GeneralConstants.FAIL_CODE, GeneralConstants.FAILED, new HashMap<
          >());
    }
  }

  @Override
  public SimpleResponse entity(Object param) {
    try {

      ObjectMapper om = new ObjectMapper();

      Map<String, Object> reqParam = om.convertValue(param, new TypeReference<>(){});

      if (reqParam.get("id") == null || reqParam.get("id").toString().equals(GeneralConstants.EMPTY_STRING)){
        return new SimpleResponse(GeneralConstants.VALIDATION_CODE, GeneralConstants.UNAUTHORIZED, new HashMap<>());
      }

      PurchaseDetailModel response = PurchaseDetailModel.findById(reqParam.get("id").toString());

      if (response == null){
        return new SimpleResponse(GeneralConstants.VALIDATION_CODE, "purchase detail does not exists", GeneralConstants.EMPTY_STRING);
      }

      return new SimpleResponse(GeneralConstants.SUCCESS_CODE, GeneralConstants.SUCCESS, response);

    }catch (Exception event){
      LOGGER.error(event.getMessage(), event);
      return new SimpleResponse(GeneralConstants.FAIL_CODE, event.getMessage(), event);
    }
  }

  @Transactional
  @TransactionConfiguration(timeout = 20)
  public SimpleResponse postInsert(Object param, String header){
    try {

      ObjectMapper om = new ObjectMapper();

      Map<String, Object> reqHeader = om.readValue(header, Map.class);

      if (reqHeader.get("userId") == null || reqHeader.get("userId").toString().equals(GeneralConstants.EMPTY_STRING)){
        return new SimpleResponse(GeneralConstants.VALIDATION_CODE,  GeneralConstants.UNAUTHORIZED, new String());
      }

      PurchaseDetailModel pd = om.convertValue(param, PurchaseDetailModel.class);

      if (pd.quantity == null){
        return new SimpleResponse(GeneralConstants.VALIDATION_CODE, "quantity is required", new String());
      }

      if (pd.purchase == null){
        return new SimpleResponse(GeneralConstants.VALIDATION_CODE, "purchase is required", new String());
      }

      if (pd.product == null){
        return new SimpleResponse(GeneralConstants.VALIDATION_CODE, "product is required", new String());
      }


      Query result = insertPurchaseDetail(pd.quantity, pd.purchase, pd.product, reqHeader.get("userId").toString(), reqHeader.get("userId").toString());

      result.executeUpdate();

      Object[] response = findBy(pd.quantity, pd.purchase, pd.product, reqHeader.get("userId").toString(), reqHeader.get("userId").toString());

      return new SimpleResponse(GeneralConstants.SUCCESS_CODE, GeneralConstants.SUCCESS, getPurchaseDetail(response[0].toString()));

    }catch (Exception event){
      LOGGER.error(event.getMessage(), event);
      return new SimpleResponse(GeneralConstants.FAIL_CODE, event.getMessage(), event);
    }
  }

  @Transactional
  @TransactionConfiguration(timeout = 20)
  public SimpleResponse postUpdate(Object param, String header){
    try {

      ObjectMapper om = new ObjectMapper();

      Map<String, Object> reqHeader = om.readValue(header, Map.class);

      if (reqHeader.get("userId") == null || reqHeader.get("userId").toString().equals(GeneralConstants.EMPTY_STRING)){
        return new SimpleResponse(GeneralConstants.VALIDATION_CODE,  GeneralConstants.UNAUTHORIZED, new String());
      }

      Map<String, Object> reqParam = om.convertValue(param, Map.class);

      if (reqParam.get("id") == null || reqParam.get("id").toString().equals(GeneralConstants.EMPTY_STRING))
        return new SimpleResponse(GeneralConstants.VALIDATION_CODE, "id is required", new String());

      PurchaseDetailModel pd = PurchaseDetailModel.findById(reqParam.get("id").toString());

      if (pd == null)
        return new SimpleResponse(GeneralConstants.VALIDATION_CODE, "purchase detail does not exist", new String());

      Query result = updatePurchaseDetail(reqParam.get("id").toString(), Integer.parseInt(reqParam.get("quantity").toString()), reqParam.get("purchase"), reqParam.get("product"), reqHeader.get("userId").toString());

      result.executeUpdate();

      return new SimpleResponse(GeneralConstants.SUCCESS_CODE, GeneralConstants.SUCCESS, getPurchaseDetail(reqParam.get("id").toString()));

    }catch (Exception event){
      LOGGER.error(event.getMessage(), event);
      return new SimpleResponse(GeneralConstants.FAIL_CODE, event.getMessage(), event);
    }
  }

  @Transactional
  public SimpleResponse postDelete(Object param, String header){
    try {

      ObjectMapper om = new ObjectMapper();

      Map<String, Object> reqHeader = om.readValue(header, Map.class);

      if (reqHeader.get("userId") == null || reqHeader.get("userId").toString().equals(GeneralConstants.EMPTY_STRING))
        return new SimpleResponse(GeneralConstants.VALIDATION_CODE,  GeneralConstants.UNAUTHORIZED, new String());


      Map<String, Object> reqParam = om.convertValue(param, Map.class);

      if (reqParam.get("id") == null || reqParam.get("id").toString().equals(GeneralConstants.EMPTY_STRING))
        return new SimpleResponse(GeneralConstants.VALIDATION_CODE, "id is required", new String());

      PurchaseDetailModel pd = PurchaseDetailModel.findById(reqParam.get("id").toString());

      if (pd == null){
        return new SimpleResponse(GeneralConstants.VALIDATION_CODE, "purchase detail does not exists", GeneralConstants.EMPTY_STRING);
      }

      deletePurchaseDetail(reqParam.get("id").toString()).executeUpdate();

      return new SimpleResponse(GeneralConstants.SUCCESS_CODE, "purchase detail has been deleted", GeneralConstants.EMPTY_STRING);

    }catch (Exception event){
      LOGGER.error(event.getMessage(), event);
      return new SimpleResponse(GeneralConstants.FAIL_CODE, event.getMessage(), event);
    }
  }

  public SimpleResponse getListPurchaseDetail(){
    try {

      StringBuilder sb = new StringBuilder();
      sb.append("select pd.id, pd.quantity, pd.purchase_id, pd.product_id");
      sb.append(" from hary.purchase_detail pd order by pd.id desc");

      Query query = em.createNativeQuery(sb.toString());

      List<Object[]> result = query.getResultList();

      List<Map<String, Object>> data = BasicUtils.createListOfMapFromArray(result, "id", "quantity", "purchase", "product");

      for (Map<String, Object> pd : data){
        pd.replace("purchase", purchaseService.getPurchase(pd.get("purchase").toString()));
        pd.replace("product", productService.getProduct(pd.get("product").toString()));
      }

      return new SimpleResponse(GeneralConstants.SUCCESS_CODE, GeneralConstants.SUCCESS, data);

    }catch (Exception event){
      LOGGER.error(event.getMessage(), event);
      return new SimpleResponse(GeneralConstants.FAIL_CODE, event.getMessage(), event);
    }
  }

  public SimpleResponse getTransactionList(){
    try {

      StringBuilder sb = new StringBuilder();

      sb.append("select s.name AS \"Supplier Name\", s.phone AS \"Supplier Phone\", pcs.date AS \"Date\", pcs.purchase_number AS \"Purchase Number\", p.\"name\"AS \"Product Name\", pd.quantity AS \"Quantity\", p.price AS \"Price\", SUM(p.price * pd.quantity) as \"Sub Total\" ");
      sb.append(" from hary.supplier s");
      sb.append(" inner join hary.purchase pcs on pcs.supplier_id = s.id");
      sb.append(" inner join hary.purchase_detail pd on pd.purchase_id = pcs.id");
      sb.append(" inner join hary.product p on p.id = pd.product_id");
      sb.append(" group by s.name, s.phone, pcs.date, pcs.purchase_number, p.name, pd.quantity, p.price");

      Query query = em.createNativeQuery(sb.toString());

      List<Map<String, Object>> response = BasicUtils.createListOfMapFromArray(query.getResultList(), "supplier_name", "supplier_phone", "date", "purchase_number", "product_name", "quantity", "price", "sub_total");

      return new SimpleResponse(GeneralConstants.SUCCESS_CODE, GeneralConstants.SUCCESS, response);

    }catch (Exception event){
      LOGGER.error(event.getMessage(), event);
      return new SimpleResponse(GeneralConstants.FAIL_CODE, GeneralConstants.FAILED, event.getMessage() + event);
    }
  }

  public SimpleResponse getTotalPurchaseProduct(){
    try {

      StringBuilder sb = new StringBuilder();

      sb.append("select p.\"name\" as \"Product Name\", sum(pd.quantity) as \"Total Quantity\", count (p.\"name\") as \"Total\" ");
      sb.append(" from hary.product p");
      sb.append(" inner join hary.purchase_detail pd on pd.product_id = p.id");
      sb.append(" inner join hary.purchase pcs on pcs.id = pd.purchase_id");
      sb.append(" where pcs.\"date\" between '2022-1-1' and '2022-12-12'");
      sb.append(" group by p.\"name\"");

      Query query = em.createNativeQuery(sb.toString());

      List<Map<String, Object>> response = BasicUtils.createListOfMapFromArray(query.getResultList(), "product_name", "total_quantity", "total");

      return new SimpleResponse(GeneralConstants.SUCCESS_CODE, GeneralConstants.SUCCESS, response);

    }catch (Exception event){
      LOGGER.error(event.getMessage(), event);
      return new SimpleResponse(GeneralConstants.FAIL_CODE, GeneralConstants.FAILED, event.getMessage() + event);
    }
  }


  public SimpleResponse getTotalPurchaseSupplier(){
    try {

      StringBuilder sb = new StringBuilder();

      sb.append(" select s.\"name\" as \"Supplier Name\", sum(pd.quantity) as \"Total\"");
      sb.append(" from hary.supplier s");
      sb.append(" inner join hary.purchase p on p.supplier_id  = s.id");
      sb.append(" inner join hary.purchase_detail pd on pd.purchase_id  = p.id");
      sb.append(" where p.\"date\" between '2022-1-1' and '2022-12-12'");
      sb.append(" group by s.\"name\"");

      Query query = em.createNativeQuery(sb.toString());

      List<Map<String, Object>> response = BasicUtils.createListOfMapFromArray(query.getResultList(), "supplier_name", "total");

      return new SimpleResponse(GeneralConstants.SUCCESS_CODE, GeneralConstants.SUCCESS, response);

    }catch (Exception event){
      LOGGER.error(event.getMessage(), event);
      return new SimpleResponse(GeneralConstants.FAIL_CODE, GeneralConstants.FAILED, event.getMessage() + event);
    }
  }

  public Object[] findBy(Integer quantity, Object purchase, Object product, String createdBy, String updateBy){
    Query findQuery = em.createNativeQuery("select id, quantity, purchase_id, product_id from hary.purchase_detail where quantity = :quantity and purchase_id = :purchase and product_id = :product and created_by = :createdBy and updated_by = :updatedBy");

    findQuery.setParameter("quantity", quantity);

    ObjectMapper om = new ObjectMapper();

    findQuery.setParameter("purchase", om.convertValue(purchase, PurchaseModel.class));
    findQuery.setParameter("product", om.convertValue(product, ProductModel.class));
    findQuery.setParameter("createdBy", createdBy);
    findQuery.setParameter("updatedBy", updateBy);

    return (Object[]) findQuery.getSingleResult();
  }

  public Query insertPurchaseDetail(Integer quantity, Object purchase, Object product, String createdBy, String updateBy) throws ParseException {
    StringBuilder sb = new StringBuilder();
    sb.append("insert into hary.purchase_detail");
    sb.append(" (id, created_at, created_by, updated_at, updated_by, \"version\", quantity, product_id, purchase_id) values");
    sb.append(" (gen_random_uuid(), pg_catalog.now(), :createdBy, pg_catalog.now(), :updatedBy, 0, :quantity, :product, :purchase)");

    Query query = em.createNativeQuery(sb.toString());
    query.setParameter("quantity", quantity);
    query.setParameter("purchase", purchase);

    query.setParameter("product", product);
    query.setParameter("createdBy", createdBy);
    query.setParameter("updatedBy", updateBy);

    return query;
  }

  public Query updatePurchaseDetail(String id, Integer quantity, Object purchase, Object product, String updatedBy) throws ParseException {
    StringBuilder sb = new StringBuilder();
    sb.append("UPDATE hary.purchase_detail");
    sb.append(" SET updated_at = pg_catalog.now(), updated_by = :updatedBy, \"version\" = :version, quantity = :quantity, product_id = :product, purchase_id = :purchase");
    sb.append(" where id = :id");

    Query query = em.createNativeQuery(sb.toString());

    if (id != null)
      query.setParameter("id", id);

    if (quantity != null)
      query.setParameter("quantity", quantity);

    ObjectMapper om = new ObjectMapper();

    if (purchase != null)
      query.setParameter("purchase", om.convertValue(purchase, PurchaseModel.class));

    if (product != null)
      query.setParameter("product", om.convertValue(product, ProductModel.class));

    if (updatedBy != null)
      query.setParameter("updatedBy", updatedBy);

    Object[] versionPurchaseDetail = getVersionPurchaseDetail(id);

    query.setParameter("version", Long.parseLong(versionPurchaseDetail[4].toString()) + 1L);

    return query;
  }

  public Query deletePurchaseDetail(String id){
    Query query = em.createNativeQuery("delete from hary.purchase_detail where id = :id");

    if (id != null)
      query.setParameter("id", id);

    return query;
  }

  public Object[] getVersionPurchaseDetail(String id){

    StringBuilder sb = new StringBuilder();
    sb.append("select id, quantity, purchase_id, product_id, version from hary.purchase_detail where id = :id");

    Query query = em.createNativeQuery(sb.toString());
    query.setParameter("id", id);

    return (Object[]) query.getSingleResult();

  }

  public Map<String, Object> getPurchaseDetail(String id) throws Exception {

    StringBuilder sb = new StringBuilder();
    sb.append("select id, quantity, purchase_id, product_id");
    sb.append(" from hary.purchase_detail");
    sb.append(" where id = :id");

    Query query = em.createNativeQuery(sb.toString());
    query.setParameter("id", id);

    Object[] result = (Object[]) query.getSingleResult();

    Map<String, Object> data = BasicUtils.createMapFromArray(result, "id","quantity", "purchase", "product");

    Map<String, Object> purchase = purchaseService.getPurchase(result[2].toString());
    Map<String, Object> product = productService.getProduct(result[3].toString());

    data.replace("purchase", purchase);
    data.replace("product", product);

    return data;
  }
}
