package com.hary.agustian.services;

import com.barrans.util.*;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.hary.agustian.models.CustomerModel;
import com.hary.agustian.models.SalesModel;
import io.quarkus.narayana.jta.runtime.TransactionConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Parameter;
import javax.persistence.Query;
import javax.transaction.Transactional;
import java.math.BigInteger;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@ApplicationScoped
public class SalesService implements IAction {

  private static final Logger LOGGER = LoggerFactory.getLogger(SalesService.class.getName());

  @Inject
  EntityManager em;

  @Inject
  CustomerService customerService;

  @Override
  @Transactional
  @TransactionConfiguration(timeout = 30)
  public SimpleResponse insert(Object param, String header) {
    try {

      ObjectMapper om = new ObjectMapper();

      Map<String, Object> reqHeader = om.readValue(header, Map.class);

      if (reqHeader.get("userId") == null || reqHeader.get("userId").toString().equals(GeneralConstants.EMPTY_STRING)){
        return new SimpleResponse(GeneralConstants.VALIDATION_CODE, GeneralConstants.UNAUTHORIZED, new HashMap<>());
      }

      SalesModel sales = om.convertValue(param, SalesModel.class);

      if (sales.transactionNumber == null || sales.transactionNumber.equals(GeneralConstants.EMPTY_STRING)){
        return new SimpleResponse(GeneralConstants.VALIDATION_CODE, "transaction number is required", new HashMap<>());
      }

      if (sales.date == null){
        return new SimpleResponse(GeneralConstants.VALIDATION_CODE, "date is required", new HashMap<>());
      }

      Map<String, Object> reqParam = om.convertValue(param, Map.class);


      if (reqParam.get("date") != null){
        sales.date = convertFromStringToDateYYYYMMDD(reqParam.get("date").toString());
      }


      ObjectActiveAndCreatedDateUtil.registerObject(sales, reqHeader.get("userId").toString());
      sales.persist();

      return new SimpleResponse(GeneralConstants.SUCCESS_CODE, GeneralConstants.SUCCESS, getSales(sales.id));

    }catch (Exception event){
      LOGGER.error(event.getMessage(), event);
      return new SimpleResponse(GeneralConstants.FAIL_CODE, GeneralConstants.FAILED, event.getMessage() + event);
    }
  }

  @Override
  @Transactional
  @TransactionConfiguration(timeout = 30)
  public SimpleResponse update(Object param, String header) {
    try {

      ObjectMapper om = new ObjectMapper();

      Map<String, Object> reqHeader = om.readValue(header, Map.class);

      if (reqHeader.get("userId") == null || reqHeader.get("userId").toString().equals(GeneralConstants.EMPTY_STRING)){
        return new SimpleResponse(GeneralConstants.VALIDATION_CODE, GeneralConstants.UNAUTHORIZED, new HashMap<>());
      }

      Map<String, Object> reqParam = om.convertValue(param, Map.class);

      if (reqParam.get("id") == null || reqParam.get("id").toString().equals(GeneralConstants.EMPTY_STRING)){
        return new SimpleResponse(GeneralConstants.VALIDATION_CODE, "sales id is required", new HashMap<>());
      }

      SalesModel sales = SalesModel.findById(reqParam.get("id").toString());

      if (sales == null){
        return new SimpleResponse(GeneralConstants.VALIDATION_CODE, "sales does not exists", new HashMap<>());
      }

      if (sales.transactionNumber != null){
        sales.transactionNumber = reqParam.get("transactionNumber").toString();
      }

      if (sales.date != null){
        sales.date = convertFromStringToDateYYYYMMDD(reqParam.get("date").toString());
      }

      if (sales.customer != null){
        sales.customer = om.convertValue(reqParam.get("customer"), CustomerModel.class);
      }

      ObjectActiveAndCreatedDateUtil.updateObject(sales, reqHeader.get("userId").toString());
      sales.persist();

      return new SimpleResponse(GeneralConstants.SUCCESS_CODE, GeneralConstants.SUCCESS, getSales(sales.id));

    }catch (Exception event){
      LOGGER.error(event.getMessage(), event);
      return new SimpleResponse(GeneralConstants.FAIL_CODE, GeneralConstants.FAILED, event + event.getMessage());
    }
  }

  @Override
  public SimpleResponse inquiry(Object param) {
    try {

      ObjectMapper om = new ObjectMapper();

      Map<String, Object> request = om.convertValue(param, new TypeReference<>(){});

      Integer limit = 5, offset = 0;

      Boolean filterTransaction = false, filterDate = false;

      if (request.containsKey("limit") && request.get("limit") != null){
        limit = Integer.parseInt(request.get("limit").toString());
      }

      if (request.containsKey("offset") && request.get("offset") != null){
        offset = Integer.parseInt(request.get("offset").toString());
      }

      StringBuilder queryString = new StringBuilder();
      queryString.append("select id, transaction_number, date from hary.sales where true");

      if (request.containsKey("transactionNumber") && request.get("transactionNumber") != null){
        queryString.append(" and transaction_number ilike :paramTransaction ");
        filterTransaction = true;
      }

      if (request.containsKey("date") && request.get("date") != null){
        queryString.append(" and \"date\" ilike :paramDate ");
        filterDate = true;
      }

      queryString.append("order by id desc");

      Query query = em.createNativeQuery(queryString.toString());

      if (filterTransaction){
        query.setParameter("paramTransaction", "%"+request.get("transactionNumber").toString()+"%");
      }

      if (filterDate){
        query.setParameter("paramDate", "%"+request.get("date").toString()+"%");
      }

      if (!limit.equals(-99) || !offset.equals(-99) || limit >= 0 || offset >=0){
        query.setMaxResults(limit);
        query.setFirstResult(offset);
      }

      List<Object[]> result = query.getResultList();

      List<Map<String, Object>> data = BasicUtils.createListOfMapFromArray(result, "id", "transaction_number", "date");

      Query qCount = em.createNativeQuery(String.format(queryString.toString().replaceFirst("select.* from", "select count (*) from").replaceFirst("order by.*", "")));

      for (Parameter queryParam : query.getParameters()){
        qCount.setParameter(queryParam.getName(), query.getParameterValue(queryParam));
      }

      BigInteger count = (BigInteger) qCount.getSingleResult();

      Map<String, Object> response = new HashMap<>();
      response.put("data", data);
      response.put("total", count);
      response.put("filtered", data.size());

      return new SimpleResponse(GeneralConstants.SUCCESS_CODE, GeneralConstants.SUCCESS, response);

    }catch (Exception event){
      LOGGER.error(event.getMessage(), event);
      return new SimpleResponse(GeneralConstants.FAIL_CODE, GeneralConstants.FAILED, new HashMap<
          >());
    }
  }

  @Override
  public SimpleResponse entity(Object param) {
    try {

      ObjectMapper om = new ObjectMapper();

      Map<String, Object> reqParam = om.convertValue(param, new TypeReference<>(){});

      if (reqParam.get("id") == null || reqParam.get("id").toString().equals(GeneralConstants.EMPTY_STRING)){
        return new SimpleResponse(GeneralConstants.VALIDATION_CODE, "id is required", new HashMap<>());
      }

      Map<String, Object> response = getSales(reqParam.get("id").toString());

      if (response.size() == 0){return new SimpleResponse(GeneralConstants.VALIDATION_CODE, "sales does not exists", new HashMap<>());
      }

      return new SimpleResponse(GeneralConstants.SUCCESS_CODE, GeneralConstants.SUCCESS, response);

    }catch (Exception event){
      LOGGER.error(event.getMessage(), event);
      return new SimpleResponse(GeneralConstants.FAIL_CODE, event.getMessage(), event);
    }
  }

  @Transactional
  @TransactionConfiguration(timeout = 20)
  public SimpleResponse postInsert(Object param, String header){
    try {

      ObjectMapper om = new ObjectMapper();

      Map<String, Object> reqHeader = om.readValue(header, Map.class);

      if (reqHeader.get("userId") == null || reqHeader.get("userId").toString().equals(GeneralConstants.EMPTY_STRING)){
        return new SimpleResponse(GeneralConstants.VALIDATION_CODE,  GeneralConstants.UNAUTHORIZED, new String());
      }

      SalesModel sales = om.convertValue(param, SalesModel.class);

      if (sales.transactionNumber == null || sales.transactionNumber.equals(GeneralConstants.EMPTY_STRING)){
        return new SimpleResponse(GeneralConstants.VALIDATION_CODE, "transaction number is required", new String());
      }

      if (sales.date == null){
        return new SimpleResponse(GeneralConstants.VALIDATION_CODE, "date is required", new String());
      }

      if (sales.customer == null){
        return new SimpleResponse(GeneralConstants.VALIDATION_CODE, "customer is required", new String());
      }


      Query result = insertSales(sales.transactionNumber, sales.date, sales.customer, reqHeader.get("userId").toString(), reqHeader.get("userId").toString());

      result.executeUpdate();

      Object[] response = findBy(sales.transactionNumber, sales.date, sales.customer, reqHeader.get("userId").toString(), reqHeader.get("userId").toString());

      return new SimpleResponse(GeneralConstants.SUCCESS_CODE, GeneralConstants.SUCCESS, getSales(response[0].toString()));

    }catch (Exception event){
      LOGGER.error(event.getMessage(), event);
      return new SimpleResponse(GeneralConstants.FAIL_CODE, event.getMessage(), event);
    }
  }

  @Transactional
  @TransactionConfiguration(timeout = 20)
  public SimpleResponse postUpdate(Object param, String header){
    try {

      ObjectMapper om = new ObjectMapper();

      Map<String, Object> reqHeader = om.readValue(header, Map.class);

      if (reqHeader.get("userId") == null || reqHeader.get("userId").toString().equals(GeneralConstants.EMPTY_STRING)){
        return new SimpleResponse(GeneralConstants.VALIDATION_CODE,  GeneralConstants.UNAUTHORIZED, new String());
      }

      Map<String, Object> reqParam = om.convertValue(param, Map.class);

      if (reqParam.get("id") == null || reqParam.get("id").toString().equals(GeneralConstants.EMPTY_STRING))
        return new SimpleResponse(GeneralConstants.VALIDATION_CODE, "id is required", new String());

      SalesModel sales = SalesModel.findById(reqParam.get("id").toString());

      if (sales == null)
        return new SimpleResponse(GeneralConstants.VALIDATION_CODE, "sales does not exist", new String());

      Query result = updateSales(reqParam.get("id").toString(), reqParam.get("transactionNumber").toString(), reqParam.get("date").toString(), reqParam.get("customer"), reqHeader.get("userId").toString());

      result.executeUpdate();

      return new SimpleResponse(GeneralConstants.SUCCESS_CODE, GeneralConstants.SUCCESS, getSales(reqParam.get("id").toString()));

    }catch (Exception event){
      LOGGER.error(event.getMessage(), event);
      return new SimpleResponse(GeneralConstants.FAIL_CODE, event.getMessage(), event);
    }
  }

  @Transactional
  public SimpleResponse postDelete(Object param, String header){
    try {

      ObjectMapper om = new ObjectMapper();

      Map<String, Object> reqHeader = om.readValue(header, Map.class);

      if (reqHeader.get("userId") == null || reqHeader.get("userId").toString().equals(GeneralConstants.EMPTY_STRING))
        return new SimpleResponse(GeneralConstants.VALIDATION_CODE,  GeneralConstants.UNAUTHORIZED, new String());


      Map<String, Object> reqParam = om.convertValue(param, Map.class);

      if (reqParam.get("id") == null || reqParam.get("id").toString().equals(GeneralConstants.EMPTY_STRING))
        return new SimpleResponse(GeneralConstants.VALIDATION_CODE, "id is required", new String());

      SalesModel sales = SalesModel.findById(reqParam.get("id").toString());

      if (sales == null){
        return new SimpleResponse(GeneralConstants.VALIDATION_CODE, "sales does not exists", GeneralConstants.EMPTY_STRING);
      }

      deleteSales(reqParam.get("id").toString()).executeUpdate();

      return new SimpleResponse(GeneralConstants.SUCCESS_CODE, "sales has been deleted", GeneralConstants.EMPTY_STRING);

    }catch (Exception event){
      LOGGER.error(event.getMessage(), event);
      return new SimpleResponse(GeneralConstants.FAIL_CODE, event.getMessage(), event);
    }
  }

  public Object[] findBy(String transactionNumber, Date date, Object customer, String createdBy, String updateBy){
    Query findQuery = em.createNativeQuery("select id, transaction_number, date, customer_id from hary.sales where transaction_number = :transactionNumber and date = :date and customer_id = :customer and created_by = :createdBy and updated_by = :updatedBy");
    findQuery.setParameter("transactionNumber", transactionNumber);
    findQuery.setParameter("date", date);

    ObjectMapper om = new ObjectMapper();

    findQuery.setParameter("customer", om.convertValue(customer, CustomerModel.class));
    findQuery.setParameter("createdBy", createdBy);
    findQuery.setParameter("updatedBy", updateBy);

    return (Object[]) findQuery.getSingleResult();
  }

  public Query insertSales(String transactionNumber, Date date, Object customer, String createdBy, String updateBy) throws ParseException {
    StringBuilder sb = new StringBuilder();
    sb.append("insert into hary.sales");
    sb.append(" (id, created_at, created_by, updated_at, updated_by, \"version\", \"date\", transaction_number, customer_id) values");
    sb.append(" (gen_random_uuid(), pg_catalog.now(), :createdBy, pg_catalog.now(), :updatedBy, 0, :date, :transactionNumber, :customer)");

    Query query = em.createNativeQuery(sb.toString());
    query.setParameter("transactionNumber", transactionNumber);
    query.setParameter("date", date);

    query.setParameter("customer", customer);
    query.setParameter("createdBy", createdBy);
    query.setParameter("updatedBy", updateBy);

    return query;
  }

  public Query updateSales(String id, String transactionNumber, String date, Object customer, String updatedBy) throws ParseException {
    StringBuilder sb = new StringBuilder();
    sb.append("UPDATE hary.sales");
    sb.append(" SET updated_at = pg_catalog.now(), updated_by = :updatedBy, \"version\" = :version, transaction_number = :transactionNumber, \"date\" = :date, customer_id= :customer");
    sb.append(" where id = :id");

    Query query = em.createNativeQuery(sb.toString());

    if (id != null)
      query.setParameter("id", id);

    if (transactionNumber != null)
      query.setParameter("transactionNumber", transactionNumber);

    if (date != null)
      query.setParameter("date", convertFromStringToDateYYYYMMDD(date));

    ObjectMapper om = new ObjectMapper();

    if (customer != null)
      query.setParameter("customer", om.convertValue(customer, CustomerModel.class));

    if (updatedBy != null)
      query.setParameter("updatedBy", updatedBy);

    Object[] versionSales = getVersionSales(id);

    query.setParameter("version", Long.parseLong(versionSales[4].toString()) + 1L);

    return query;
  }

  public Query deleteSales(String id){
    Query query = em.createNativeQuery("delete from hary.sales where id = :id");

    if (id != null)
      query.setParameter("id", id);

    return query;
  }

  public Object[] getVersionSales(String id){

    StringBuilder sb = new StringBuilder();
    sb.append("select id, transaction_number, date, customer_id, version from hary.sales where id = :id");

    Query query = em.createNativeQuery(sb.toString());
    query.setParameter("id", id);

    return (Object[]) query.getSingleResult();

  }

  public Map<String, Object> getSales(String id) throws Exception {

    StringBuilder sb = new StringBuilder();
    sb.append("select id, transaction_number, date, customer_id ");
    sb.append(" from hary.sales");
    sb.append(" where id = :id");

    Query query = em.createNativeQuery(sb.toString());
    query.setParameter("id", id);

    Object[] result = (Object[]) query.getSingleResult();

    Map<String, Object> data = BasicUtils.createMapFromArray(result, "id","transaction_number", "date", "customer");

    Map<String, Object> customer = customerService.getCustomer(result[3].toString());

    data.replace("customer", customer);


    return data;
  }

  private Date convertFromStringToDateYYYYMMDD(String date) throws ParseException {
    return new SimpleDateFormat(DateUtil.DATE_PATTERN).parse(date);
  }

}
