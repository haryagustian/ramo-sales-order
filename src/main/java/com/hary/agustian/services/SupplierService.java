package com.hary.agustian.services;

import com.barrans.util.*;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.hary.agustian.models.SupplierModel;
import io.quarkus.narayana.jta.runtime.TransactionConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Parameter;
import javax.persistence.Query;
import javax.transaction.Transactional;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@ApplicationScoped
public class SupplierService implements IAction {

  private static final Logger LOGGER = LoggerFactory.getLogger(SupplierService.class.getName());

  @Inject
  EntityManager em;

  @Override
  @Transactional
  @TransactionConfiguration(timeout = 20)
  public SimpleResponse insert(Object o, String s) {
    try {

      ObjectMapper om = new ObjectMapper();

      Map<String, Object> reqHeader = om.readValue(s, Map.class);

      if (reqHeader.get("userId") == null || reqHeader.get("userId").toString().equals(GeneralConstants.EMPTY_STRING)){
        return new SimpleResponse(GeneralConstants.VALIDATION_CODE, GeneralConstants.UNAUTHORIZED, new HashMap<>());
      }

      SupplierModel supplier = om.convertValue(o, SupplierModel.class);

      if (supplier.name == null || supplier.name.equals(GeneralConstants.EMPTY_STRING)){
        return new SimpleResponse(GeneralConstants.VALIDATION_CODE, "name is required", new HashMap<>());
      }

      if (supplier.address == null || supplier.address.equals(GeneralConstants.EMPTY_STRING)){
        return new SimpleResponse(GeneralConstants.VALIDATION_CODE, "address is required", new HashMap<>());
      }

      if (supplier.phone == null || supplier.phone.equals(GeneralConstants.EMPTY_STRING)){
        return new SimpleResponse(GeneralConstants.VALIDATION_CODE, "phone is required", new HashMap<>());
      }

      if (supplier.mobile == null || supplier.mobile.equals(GeneralConstants.EMPTY_STRING)){
        return new SimpleResponse(GeneralConstants.VALIDATION_CODE, "mobile is required", new HashMap<>());
      }

      ObjectActiveAndCreatedDateUtil.registerObject(supplier, reqHeader.get("userId").toString());
      supplier.persist();

      return new SimpleResponse(GeneralConstants.SUCCESS_CODE, GeneralConstants.SUCCESS, getSupplier(supplier.id));

    }catch (Exception event){
      LOGGER.error(event.getMessage(), event);
      return new SimpleResponse(GeneralConstants.FAIL_CODE, GeneralConstants.FAILED, event.getMessage()  + event);
    }
  }

  @Override
  @Transactional
  @TransactionConfiguration(timeout = 20)
  public SimpleResponse update(Object o, String s) {
    try {

      ObjectMapper om = new ObjectMapper();

      Map<String, Object> reqHeader = om.readValue(s, Map.class);

      if (reqHeader.get("userId") == null || reqHeader.get("userId").toString().equals(GeneralConstants.EMPTY_STRING)){
        return new SimpleResponse(GeneralConstants.VALIDATION_CODE, GeneralConstants.UNAUTHORIZED, new HashMap<>());
      }

      Map<String, Object> reqParam = om.convertValue(o, Map.class);

      if (reqParam.get("id") == null || reqParam.get("id").toString().equals(GeneralConstants.EMPTY_STRING)){
        return new SimpleResponse(GeneralConstants.VALIDATION_CODE, "id is required", new HashMap<>());
      }

      SupplierModel supplier = SupplierModel.findById(reqParam.get("id").toString());

      if (supplier == null){
        return new SimpleResponse(GeneralConstants.VALIDATION_CODE, "supplier does not exists", new HashMap<>());
      }

      if (reqParam.get("name") != null){
        supplier.name = reqParam.get("name").toString();
      }

      if (reqParam.get("address") != null){
        supplier.address = reqParam.get("address").toString();
      }

      if (reqParam.get("phone") != null){
        supplier.phone = reqParam.get("phone").toString();
      }

      if (reqParam.get("mobile") != null){
        supplier.mobile = reqParam.get("mobile").toString();
      }

      ObjectActiveAndCreatedDateUtil.updateObject(supplier, reqHeader.get("userId").toString(), supplier.isActive);
      supplier.persist();

      return new SimpleResponse(GeneralConstants.SUCCESS_CODE, GeneralConstants.SUCCESS, getSupplier(supplier.id));

    }catch (Exception event){
      LOGGER.error(event.getMessage(), event);
      return new SimpleResponse(GeneralConstants.FAIL_CODE, GeneralConstants.FAILED, event.getMessage() + event);
    }
  }

  @Override
  public SimpleResponse inquiry(Object o) {
    try {

      ObjectMapper om = new ObjectMapper();
      Map<String, Object> request = om.convertValue(o, new TypeReference<>(){});

      Boolean filterPhone = false, filterName = false,
          filterAddress = false, filterMobile = false;

      Integer limit = 5, offset = 0;


      StringBuilder queryString = new StringBuilder();
      queryString.append("select id, name, address, phone, mobile from hary.supplier where true ");


      if (request.containsKey("limit") && null != request.get("limit"))
        limit = Integer.parseInt(request.get("limit").toString());

      if (request.containsKey("offset") && null != request.get("offset"))
        offset = Integer.parseInt(request.get("offset").toString());

      if(request.containsKey("name") && request.get("name") != null) {
        queryString.append(" and \"name\" ilike :paramName ");
        filterName = true;
      }

      if(request.containsKey("phone") && request.get("phone") != null) {
        queryString.append(" and phone ilike :paramPhone ");
        filterPhone = true;
      }

      if(request.containsKey("address") && request.get("address") != null) {
        queryString.append(" and address ilike :paramAddress ");
        filterAddress = true;
      }

      if(request.containsKey("mobile") && request.get("mobile") != null) {
        queryString.append(" and mobile ilike :paramMobile ");
        filterMobile = true;
      }

      queryString.append(" order by id desc");

      Query query = em.createNativeQuery(queryString.toString());

      if (filterName)
        query.setParameter("paramName", "%" + request.get("name").toString() + "%");

      if (filterPhone)
        query.setParameter("paramPhone", "%" + request.get("phone").toString() + "%");

      if (filterAddress)
        query.setParameter("paramAddress", "%" + request.get("address").toString() + "%");

      if (filterMobile)
        query.setParameter("paramMobile", "%" + request.get("mobile").toString() + "%");

      if (!limit.equals(-99) || !offset.equals(-99) || limit >= 0 || offset >= 0) {
        query.setFirstResult(offset);
        query.setMaxResults(limit);
      }

      List<Object[]> result = query.getResultList();
      List<Map<String,Object>> data =
          BasicUtils.createListOfMapFromArray(
              result,
              "id",
              "name",
              "address",
              "phone",
              "mobile"
          );

      Query qCount = em.createNativeQuery(String.format(queryString.toString()
          .replaceFirst("select.* from", "select count(*) from ").replaceFirst("order by.*", "")));

      for (Parameter parameter : query.getParameters())
        qCount.setParameter(parameter.getName(), query.getParameterValue(parameter));

      BigInteger count = (BigInteger) qCount.getSingleResult();

      Map<String,Object> response = new HashMap<>();
      response.put("data", data);
      response.put("total", count);
      response.put("filtered", data.size());

      return new SimpleResponse(GeneralConstants.SUCCESS_CODE, GeneralConstants.SUCCESS, response);
    }catch (Exception event){
      LOGGER.error(event.getMessage(), event);
      return new SimpleResponse(GeneralConstants.FAIL_CODE, GeneralConstants.FAILED, new String());
    }
  }

  @Override
  public SimpleResponse entity(Object o) {
    try {

      ObjectMapper om = new ObjectMapper();

      Map<String, Object> reqParam = om.convertValue(o, Map.class);

      if (reqParam.get("id") == null || reqParam.get("id").toString().equals(GeneralConstants.EMPTY_STRING)){
        return new SimpleResponse(GeneralConstants.VALIDATION_CODE, "id is required", new HashMap<>());
      }

      Map<String, Object> response = getSupplier(reqParam.get("id").toString());

      if (response.size() == 0){
        return new SimpleResponse(GeneralConstants.VALIDATION_CODE, "supplier does not exists", new HashMap<>());
      }

      return new SimpleResponse(GeneralConstants.SUCCESS_CODE, GeneralConstants.SUCCESS, response);

    }catch (Exception event){
      LOGGER.error(event.getMessage(), event);
      return new SimpleResponse(GeneralConstants.FAIL_CODE, GeneralConstants.FAILED, event.getMessage() + event);
    }
  }

  @Transactional
  @TransactionConfiguration(timeout = 20)
  public SimpleResponse postInsert(Object param, String header){
    try {

      ObjectMapper om = new ObjectMapper();

      Map<String, Object> reqHeader = om.readValue(header, Map.class);

      if (reqHeader.get("userId") == null || reqHeader.get("userId").toString().equals(GeneralConstants.EMPTY_STRING)){
        return new SimpleResponse(GeneralConstants.VALIDATION_CODE,  GeneralConstants.UNAUTHORIZED, new String());
      }

      SupplierModel supplier = om.convertValue(param, SupplierModel.class);

      if (supplier.name == null || supplier.name.equals(GeneralConstants.EMPTY_STRING)){
        return new SimpleResponse(GeneralConstants.VALIDATION_CODE, "name is required", new String());
      }

      if (supplier.phone == null || supplier.phone.equals(GeneralConstants.EMPTY_STRING)){
        return new SimpleResponse(GeneralConstants.VALIDATION_CODE, "phone is required", new String());
      }

      if (supplier.address == null || supplier.address.equals(GeneralConstants.EMPTY_STRING)){
        return new SimpleResponse(GeneralConstants.VALIDATION_CODE, "address is required", new String());
      }

      if (supplier.mobile == null || supplier.mobile.equals(GeneralConstants.EMPTY_STRING)){
        return new SimpleResponse(GeneralConstants.VALIDATION_CODE, "mobile is required", new String());
      }

      Query result = insertSupplier(supplier.name, supplier.phone, supplier.address, supplier.mobile, reqHeader.get("userId").toString(), reqHeader.get("userId").toString());

      result.executeUpdate();

      Object[] response = findBy(supplier.name, supplier.phone, supplier.address, supplier.mobile, reqHeader.get("userId").toString(), reqHeader.get("userId").toString());

      return new SimpleResponse(GeneralConstants.SUCCESS_CODE, GeneralConstants.SUCCESS, getSupplier(response[0].toString()));

    }catch (Exception event){
      LOGGER.error(event.getMessage(), event);
      return new SimpleResponse(GeneralConstants.FAIL_CODE, event.getMessage(), event);
    }
  }

  @Transactional
  @TransactionConfiguration(timeout = 20)
  public SimpleResponse postUpdate(Object param, String header){
    try {

      ObjectMapper om = new ObjectMapper();

      Map<String, Object> reqHeader = om.readValue(header, Map.class);

      if (reqHeader.get("userId") == null || reqHeader.get("userId").toString().equals(GeneralConstants.EMPTY_STRING)){
        return new SimpleResponse(GeneralConstants.VALIDATION_CODE,  GeneralConstants.UNAUTHORIZED, new String());
      }

      Map<String, Object> reqParam = om.convertValue(param, Map.class);

      if (reqParam.get("id") == null || reqParam.get("id").toString().equals(GeneralConstants.EMPTY_STRING))
        return new SimpleResponse(GeneralConstants.VALIDATION_CODE, "id is required", new String());

      SupplierModel supplier = SupplierModel.findById(reqParam.get("id").toString());

      if (supplier == null)
        return new SimpleResponse(GeneralConstants.VALIDATION_CODE, "supplier does not exist", new String());

      Query result = updateSupplier(reqParam.get("id").toString(), reqParam.get("name").toString(), reqParam.get("phone").toString(), reqParam.get("address").toString(), reqParam.get("mobile").toString(), reqHeader.get("userId").toString());

      result.executeUpdate();

      return new SimpleResponse(GeneralConstants.SUCCESS_CODE, GeneralConstants.SUCCESS, getSupplier(reqParam.get("id").toString()));

    }catch (Exception event){
      LOGGER.error(event.getMessage(), event);
      return new SimpleResponse(GeneralConstants.FAIL_CODE, event.getMessage(), event);
    }
  }

  @Transactional
  public SimpleResponse postDelete(Object param, String header){
    try {

      ObjectMapper om = new ObjectMapper();

      Map<String, Object> reqHeader = om.readValue(header, Map.class);

      if (reqHeader.get("userId") == null || reqHeader.get("userId").toString().equals(GeneralConstants.EMPTY_STRING))
        return new SimpleResponse(GeneralConstants.VALIDATION_CODE,  GeneralConstants.UNAUTHORIZED, new String());


      Map<String, Object> reqParam = om.convertValue(param, Map.class);

      if (reqParam.get("id") == null || reqParam.get("id").toString().equals(GeneralConstants.EMPTY_STRING))
        return new SimpleResponse(GeneralConstants.VALIDATION_CODE, "id is required", new String());

      SupplierModel supplier = SupplierModel.findById(reqParam.get("id").toString());

      if (supplier == null){
        return new SimpleResponse(GeneralConstants.VALIDATION_CODE, "supplier does not exists", GeneralConstants.EMPTY_STRING);
      }

      deleteSupplier(reqParam.get("id").toString()).executeUpdate();

      return new SimpleResponse(GeneralConstants.SUCCESS_CODE, "supplier has been deleted", GeneralConstants.EMPTY_STRING);

    }catch (Exception event){
      LOGGER.error(event.getMessage(), event);
      return new SimpleResponse(GeneralConstants.FAIL_CODE, event.getMessage(), event);
    }
  }

  public SimpleResponse getListSupplier(){
    try {

      StringBuilder sb = new StringBuilder();
      sb.append("select s.name, s.address, s.phone, s.mobile from hary.supplier s");

      Query query = em.createNativeQuery(sb.toString());

      List<Object[]> result = query.getResultList();

      if (result.size() == 0){
        return new SimpleResponse(GeneralConstants.SUCCESS_CODE, "supplier there's not yet", new HashMap<>());
      }

      List<Map<String, Object>> data = BasicUtils.createListOfMapFromArray(result,  "name", "address", "phone", "mobile");

      Map<String, Object> response = new HashMap<>();
      response.put("suppliers", data);

      return new SimpleResponse(GeneralConstants.SUCCESS_CODE, GeneralConstants.SUCCESS, response);

    }catch (Exception event){
      LOGGER.error(event.getMessage(), event);
      return new SimpleResponse(GeneralConstants.FAIL_CODE, GeneralConstants.FAILED, event.getMessage() + event);
    }
  }

  public Object[] getVersionSupplier(String id){

    StringBuilder sb = new StringBuilder();
    sb.append("select id, name, address, phone, mobile, version from hary.supplier where id = :id");

    Query query = em.createNativeQuery(sb.toString());
    query.setParameter("id", id);

    return (Object[]) query.getSingleResult();

  }

  public Object[] findBy(String name, String phone, String address, String mobile, String createdBy, String updateBy){
    Query findQuery = em.createNativeQuery("select id, name, address, phone, mobile from hary.supplier where name = :name and phone = :phone and address = :address and created_by = :createdBy and updated_by = :updatedBy and mobile = :mobile");
    findQuery.setParameter("name", name);
    findQuery.setParameter("phone", phone);
    findQuery.setParameter("mobile", mobile);
    findQuery.setParameter("address", address);
    findQuery.setParameter("createdBy", createdBy);
    findQuery.setParameter("updatedBy", updateBy);

    return (Object[]) findQuery.getSingleResult();
  }

  public Query insertSupplier(String name, String phone, String address, String mobile, String createdBy, String updateBy){
    StringBuilder sb = new StringBuilder();
    sb.append("insert into hary.supplier");
    sb.append(" (id, active_at, created_at, created_by, inactive_at, is_active, updated_at, updated_by, \"version\", address, mobile, name, phone) values");
    sb.append(" (gen_random_uuid(), pg_catalog.now(), pg_catalog.now(), :createdBy, null, true, pg_catalog.now(), :updatedBy, 0, :address, :mobile, :name, :phone)");

    Query query = em.createNativeQuery(sb.toString());
    query.setParameter("name", name);
    query.setParameter("phone", phone);
    query.setParameter("mobile", mobile);
    query.setParameter("address", address);
    query.setParameter("createdBy", createdBy);
    query.setParameter("updatedBy", updateBy);

    return query;
  }

  public Query updateSupplier(String id, String name, String phone, String address, String mobile, String updatedBy){
    StringBuilder sb = new StringBuilder();
    sb.append("UPDATE hary.supplier");
    sb.append(" SET updated_at = pg_catalog.now(), updated_by = :updatedBy, \"version\"= :paramVersion, address = :address, \"name\" = :name, phone = :phone, mobile = :mobile");
    sb.append(" where id = :id");

    Query query = em.createNativeQuery(sb.toString());

    if (id != null)
      query.setParameter("id", id);

    if (name != null)
      query.setParameter("name", name);

    if (phone != null)
      query.setParameter("phone", phone);

    if (mobile != null)
      query.setParameter("mobile", mobile);

    if (address != null)
      query.setParameter("address", address);

    if (updatedBy != null)
      query.setParameter("updatedBy", updatedBy);

    Object[] versionSupplier = getVersionSupplier(id);

    query.setParameter("paramVersion", Long.parseLong(versionSupplier[5].toString()) + 1L);

    return query;
  }

  public Query deleteSupplier(String id){
    Query query = em.createNativeQuery("delete from hary.supplier where id = :id");

    if (id != null)
      query.setParameter("id", id);

    return query;
  }

  public Map<String, Object> getSupplier(String id) throws Exception {

    StringBuilder sb = new StringBuilder();
    sb.append("select id, name, address, phone, mobile");
    sb.append(" from hary.supplier");
    sb.append(" where id = :id");

    Query query = em.createNativeQuery(sb.toString());
    query.setParameter("id", id);

    Object[] result = (Object[]) query.getSingleResult();

    Map<String, Object> data = new HashMap<>();
    data = BasicUtils.createMapFromArray(result, "id", "name", "address", "phone", "mobile");

    return data;
  }

}
