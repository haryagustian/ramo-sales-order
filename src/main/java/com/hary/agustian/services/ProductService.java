package com.hary.agustian.services;

import com.barrans.util.*;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.hary.agustian.models.ProductModel;
import io.quarkus.narayana.jta.runtime.TransactionConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Parameter;
import javax.persistence.Query;
import javax.transaction.Transactional;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@ApplicationScoped
public class ProductService implements IAction {

  private static final Logger LOGGER = LoggerFactory.getLogger(ProductService.class.getName());

  @Inject
  EntityManager em;

  @Override
  @Transactional
  @TransactionConfiguration(timeout = 30)
  public SimpleResponse insert(Object param, String header) {
    try {

      ObjectMapper om = new ObjectMapper();

      Map<String, Object> reqHeader = om.readValue(header, Map.class);

      if (reqHeader.get("userId") == null || reqHeader.get("userId").toString().equals(GeneralConstants.EMPTY_STRING)){
        return new SimpleResponse(GeneralConstants.VALIDATION_CODE, GeneralConstants.UNAUTHORIZED, new HashMap<>());
      }

      ProductModel product = om.convertValue(param, ProductModel.class);

      if (product.name == null || product.name.equals(GeneralConstants.EMPTY_STRING)){
        return new SimpleResponse(GeneralConstants.VALIDATION_CODE, "product name is required", new HashMap<>());
      }

      if (product.description == null || product.description.equals(GeneralConstants.EMPTY_STRING)){
        return new SimpleResponse(GeneralConstants.VALIDATION_CODE, "product description is required", new HashMap<>());
      }

      if (product.price == null || product.price.equals(GeneralConstants.EMPTY_STRING)){
        return new SimpleResponse(GeneralConstants.VALIDATION_CODE, "product price is required", new HashMap<>());
      }

      ObjectActiveAndCreatedDateUtil.registerObject(product, reqHeader.get("userId").toString());
      product.persist();

      return new SimpleResponse(GeneralConstants.SUCCESS_CODE, GeneralConstants.SUCCESS, getProduct(product.id));

    }catch (Exception event){
      LOGGER.error(event.getMessage(), event);
      return new SimpleResponse(GeneralConstants.FAIL_CODE, GeneralConstants.FAILED, event.getMessage() + event);
    }
  }

  @Override
  @Transactional
  @TransactionConfiguration(timeout = 30)
  public SimpleResponse update(Object param, String header) {
    try {

      ObjectMapper om = new ObjectMapper();

      Map<String, Object> reqHeader = om.readValue(header, Map.class);

      if (reqHeader.get("userId") == null || reqHeader.get("userId").toString().equals(GeneralConstants.EMPTY_STRING)){
        return new SimpleResponse(GeneralConstants.VALIDATION_CODE, GeneralConstants.UNAUTHORIZED, new HashMap<>());
      }

      Map<String, Object> reqParam = om.convertValue(param, Map.class);

      if (reqParam.get("id") == null || reqParam.get("id").toString().equals(GeneralConstants.EMPTY_STRING)){
        return new SimpleResponse(GeneralConstants.VALIDATION_CODE, "product id is required", new HashMap<>());
      }

      ProductModel product = ProductModel.findById(reqParam.get("id").toString());

      if (product == null){
        return new SimpleResponse(GeneralConstants.VALIDATION_CODE, "product does not exists", new HashMap<>());
      }

      if (product.name != null){
        product.name = reqParam.get("name").toString();
      }

      if (product.description != null){
        product.description = reqParam.get("description").toString();
      }

      if (product.price != null){
        product.price = Integer.parseInt(reqParam.get("price").toString());
      }

      ObjectActiveAndCreatedDateUtil.updateObject(product, reqHeader.get("userId").toString(), product.isActive);
      product.persist();

      return new SimpleResponse(GeneralConstants.SUCCESS_CODE, GeneralConstants.SUCCESS, getProduct(product.id));

    }catch (Exception event){
      LOGGER.error(event.getMessage(), event);
      return new SimpleResponse(GeneralConstants.FAIL_CODE, GeneralConstants.FAILED, event + event.getMessage());
    }
  }

  @Override
  public SimpleResponse inquiry(Object param) {
    try {

      ObjectMapper om = new ObjectMapper();

      Map<String, Object> request = om.convertValue(param, new TypeReference<>(){});

      Integer limit = 5, offset = 0;

      Boolean filterName = false, filterDescription = false, filterPrice = false;

      if (request.containsKey("limit") && request.get("limit") != null){
        limit = Integer.parseInt(request.get("limit").toString());
      }

      if (request.containsKey("offset") && request.get("offset") != null){
        offset = Integer.parseInt(request.get("offset").toString());
      }

      StringBuilder queryString = new StringBuilder();
      queryString.append("select id, name, description, price from hary.product where true");

      if (request.containsKey("name") && request.get("name") != null){
        queryString.append(" and \"name\" ilike :paramName ");
        filterName = true;
      }

      if (request.containsKey("description") && request.get("description") != null){
        queryString.append(" and \"description\" ilike :reqDescription ");
        filterDescription = true;
      }

      if (request.containsKey("price") && request.get("price") != null){
        queryString.append(" and price = :paramPrice ");
        filterPrice = true;
      }

      queryString.append("order by id desc");

      Query query = em.createNativeQuery(queryString.toString());
      
      if (filterName){
        query.setParameter("paramName", "%"+request.get("name").toString()+"%");
      }
      
      if (filterDescription){
        query.setParameter("paramDescription", "%"+request.get("description").toString()+"%");
      }

      if (filterPrice){
        query.setParameter("paramPrice",  Integer.parseInt(request.get("price").toString()));
      }

      if (!limit.equals(-99) || !offset.equals(-99) || limit >= 0 || offset >=0){
        query.setMaxResults(limit);
        query.setFirstResult(offset);
      }

      List<Object[]> result = query.getResultList();

      List<Map<String, Object>> data = BasicUtils.createListOfMapFromArray(result, "id", "name", "description", "price");

      Query qCount = em.createNativeQuery(String.format(queryString.toString().replaceFirst("select.* from", "select count (*) from").replaceFirst("order by.*", "")));

      for (Parameter queryParam : query.getParameters()){
        qCount.setParameter(queryParam.getName(), query.getParameterValue(queryParam));
      }

      BigInteger count = (BigInteger) qCount.getSingleResult();

      Map<String, Object> response = new HashMap<>();
      response.put("data", data);
      response.put("total", count);
      response.put("filtered", data.size());

      return new SimpleResponse(GeneralConstants.SUCCESS_CODE, GeneralConstants.SUCCESS, response);

    }catch (Exception event){
      LOGGER.error(event.getMessage(), event);
      return new SimpleResponse(GeneralConstants.FAIL_CODE, GeneralConstants.FAILED, new HashMap<
          >());
    }
  }

  @Override
  public SimpleResponse entity(Object param) {
    try {

      ObjectMapper om = new ObjectMapper();

      Map<String, Object> reqParam = om.convertValue(param, new TypeReference<>(){});

      if (reqParam.get("id") == null || reqParam.get("id").toString().equals(GeneralConstants.EMPTY_STRING)){
        return new SimpleResponse(GeneralConstants.VALIDATION_CODE, "id is required", new HashMap<>());
      }

      ProductModel response = ProductModel.findById(reqParam.get("id").toString());

      if (response == null){
        return new SimpleResponse(GeneralConstants.VALIDATION_CODE, "product does not exists", new HashMap<>());
      }

      return new SimpleResponse(GeneralConstants.SUCCESS_CODE, GeneralConstants.SUCCESS, getProduct(response.id));

    }catch (Exception event){
      LOGGER.error(event.getMessage(), event);
      return new SimpleResponse(GeneralConstants.FAIL_CODE, event.getMessage(), event);
    }
  }

  @Transactional
  @TransactionConfiguration(timeout = 20)
  public SimpleResponse postInsert(Object param, String header){
    try {

      ObjectMapper om = new ObjectMapper();

      Map<String, Object> reqHeader = om.readValue(header, Map.class);

      if (reqHeader.get("userId") == null || reqHeader.get("userId").toString().equals(GeneralConstants.EMPTY_STRING)){
        return new SimpleResponse(GeneralConstants.VALIDATION_CODE,  GeneralConstants.UNAUTHORIZED, new String());
      }

      ProductModel product = om.convertValue(param, ProductModel.class);

      if (product.name == null || product.name.equals(GeneralConstants.EMPTY_STRING)){
        return new SimpleResponse(GeneralConstants.VALIDATION_CODE, "name is required", new String());
      }

      if (product.description == null || product.description.equals(GeneralConstants.EMPTY_STRING)){
        return new SimpleResponse(GeneralConstants.VALIDATION_CODE, "description is required", new String());
      }

      if (product.price == null ){
        return new SimpleResponse(GeneralConstants.VALIDATION_CODE, "price is required", new String());
      }

      Query result = insertProduct(product.name, product.description, product.price, reqHeader.get("userId").toString(), reqHeader.get("userId").toString());

      result.executeUpdate();

      Object[] response = findBy(product.name, product.description, product.price, reqHeader.get("userId").toString(), reqHeader.get("userId").toString());

      return new SimpleResponse(GeneralConstants.SUCCESS_CODE, GeneralConstants.SUCCESS, getProduct(response[0].toString()));

    }catch (Exception event){
      LOGGER.error(event.getMessage(), event);
      return new SimpleResponse(GeneralConstants.FAIL_CODE, event.getMessage(), event);
    }
  }

  @Transactional
  @TransactionConfiguration(timeout = 20)
  public SimpleResponse postUpdate(Object param, String header){
    try {

      ObjectMapper om = new ObjectMapper();

      Map<String, Object> reqHeader = om.readValue(header, Map.class);

      if (reqHeader.get("userId") == null || reqHeader.get("userId").toString().equals(GeneralConstants.EMPTY_STRING)){
        return new SimpleResponse(GeneralConstants.VALIDATION_CODE,  GeneralConstants.UNAUTHORIZED, new String());
      }

      Map<String, Object> reqParam = om.convertValue(param, Map.class);

      if (reqParam.get("id") == null || reqParam.get("id").toString().equals(GeneralConstants.EMPTY_STRING))
        return new SimpleResponse(GeneralConstants.VALIDATION_CODE, "id is required", new String());

      ProductModel product = ProductModel.findById(reqParam.get("id").toString());

      if (product == null)
        return new SimpleResponse(GeneralConstants.VALIDATION_CODE, "product does not exist", new String());

      Query result = updateProduct(reqParam.get("id").toString(), reqParam.get("name").toString(), reqParam.get("description").toString(), Integer.parseInt(reqParam.get("price").toString()), reqHeader.get("userId").toString());

      result.executeUpdate();

      return new SimpleResponse(GeneralConstants.SUCCESS_CODE, GeneralConstants.SUCCESS, getProduct(reqParam.get("id").toString()));

    }catch (Exception event){
      LOGGER.error(event.getMessage(), event);
      return new SimpleResponse(GeneralConstants.FAIL_CODE, event.getMessage(), event);
    }
  }

  @Transactional
  public SimpleResponse postDelete(Object param, String header){
    try {

      ObjectMapper om = new ObjectMapper();

      Map<String, Object> reqHeader = om.readValue(header, Map.class);

      if (reqHeader.get("userId") == null || reqHeader.get("userId").toString().equals(GeneralConstants.EMPTY_STRING))
        return new SimpleResponse(GeneralConstants.VALIDATION_CODE,  GeneralConstants.UNAUTHORIZED, new String());


      Map<String, Object> reqParam = om.convertValue(param, Map.class);

      if (reqParam.get("id") == null || reqParam.get("id").toString().equals(GeneralConstants.EMPTY_STRING))
        return new SimpleResponse(GeneralConstants.VALIDATION_CODE, "id is required", new String());

      ProductModel product = ProductModel.findById(reqParam.get("id").toString());

      if (product == null){
        return new SimpleResponse(GeneralConstants.VALIDATION_CODE, "product does not exists", GeneralConstants.EMPTY_STRING);
      }

      deleteProduct(reqParam.get("id").toString()).executeUpdate();

      return new SimpleResponse(GeneralConstants.SUCCESS_CODE, "product has been deleted", GeneralConstants.EMPTY_STRING);

    }catch (Exception event){
      LOGGER.error(event.getMessage(), event);
      return new SimpleResponse(GeneralConstants.FAIL_CODE, event.getMessage(), event);
    }
  }

  public SimpleResponse getListProduct(){
    try {

      StringBuilder sb = new StringBuilder();

      sb.append("select p.id , p.\"name\", p.description , p.price");
      sb.append(" from hary.product p");
      sb.append(" order by p.id desc");

      Query query = em.createNativeQuery(sb.toString());

      List<Map<String, Object>> response = BasicUtils.createListOfMapFromArray(query.getResultList(), "id", "name", "description", "price");

      return new SimpleResponse(GeneralConstants.SUCCESS_CODE, GeneralConstants.SUCCESS, response);

    }catch (Exception event){
      LOGGER.error(event.getMessage(), event);
      return new SimpleResponse(GeneralConstants.FAIL_CODE, GeneralConstants.FAILED, event.getMessage() + event);
    }
  }

  public Object[] getVersionProduct(String id){

    StringBuilder sb = new StringBuilder();
    sb.append("select id, name, description, price, version from hary.product where id = :id");

    Query query = em.createNativeQuery(sb.toString());
    query.setParameter("id", id);

    return (Object[]) query.getSingleResult();

  }

  public Object[] findBy(String name, String description, Integer price, String createdBy, String updateBy){
    Query findQuery = em.createNativeQuery("select id, name, description, price from hary.product where name = :name and description = :description and price = :price and created_by = :createdBy and updated_by = :updatedBy");
    findQuery.setParameter("name", name);
    findQuery.setParameter("description", description);
    findQuery.setParameter("price", price);
    findQuery.setParameter("createdBy", createdBy);
    findQuery.setParameter("updatedBy", updateBy);

    return (Object[]) findQuery.getSingleResult();
  }

  public Query insertProduct(String name, String description, Integer price, String createdBy, String updateBy){
    StringBuilder sb = new StringBuilder();
    sb.append("insert into hary.product");
    sb.append(" (id, active_at, created_at, created_by, inactive_at, is_active, updated_at, updated_by, \"version\", description, \"name\", price)");
    sb.append(" values (gen_random_uuid(), pg_catalog.now(), pg_catalog.now(), :created, null, true, pg_catalog.now(), :updated, 0, :description, :name, :price)");

    Query query = em.createNativeQuery(sb.toString());
    query.setParameter("created", createdBy);
    query.setParameter("updated", updateBy);
    query.setParameter("description", description);
    query.setParameter("name", name);
    query.setParameter("price", price);

    return query;
  }

  public Query updateProduct(String id, String name, String description, Integer price, String updatedBy){
    StringBuilder sb = new StringBuilder();
    sb.append("UPDATE hary.product");
    sb.append(" SET updated_at = pg_catalog.now(), updated_by = :paramUpdated, \"version\"= :paramVersion, description = :paramDescription, \"name\" = :paramName, price = :paramPrice");
    sb.append(" where id = :id");

    Query query = em.createNativeQuery(sb.toString());

    if (id != null)
      query.setParameter("id", id);

    if (name != null)
      query.setParameter("paramName", name);

    if (description != null)
      query.setParameter("paramDescription", description);

    if (price != null)
      query.setParameter("paramPrice", price);

    if (updatedBy != null)
      query.setParameter("paramUpdated", updatedBy);

    Object[] versionProduct = getVersionProduct(id);

    query.setParameter("paramVersion", Long.parseLong(versionProduct[4].toString()) + 1L);

    return query;
  }

  public Query deleteProduct(String id){
    Query query = em.createNativeQuery("delete from hary.product where id = :id");

    if (id != null)
      query.setParameter("id", id);

    return query;
  }

  public Map<String, Object> getProduct(String id) throws Exception {
    StringBuilder sb = new StringBuilder();
    sb.append("select id, name, description, price");
    sb.append(" from hary.product");
    sb.append(" where id = :id");

    Query query = em.createNativeQuery(sb.toString());

    query.setParameter("id", id);

    Object[] result = (Object[]) query.getSingleResult();

    Map<String, Object> product = BasicUtils
        .createMapFromArray(result, "id",
            "name",
            "description",
            "price");

    return product;

//    ProductModel product = ProductModel.findById(id);
//
//    Map<String, Object> result = new HashMap<>();
//
//    if (product != null){
//      result.put("id", product.id);
//      result.put("name", product.name);
//      result.put("description", product.description);
//      result.put("price", product.price);
//    }
//    return result;
  }
}
