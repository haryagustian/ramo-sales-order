package com.hary.agustian.services;

import com.barrans.util.*;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.hary.agustian.models.CustomerModel;
import com.hary.agustian.models.PurchaseModel;
import com.hary.agustian.models.SupplierModel;
import io.quarkus.narayana.jta.runtime.TransactionConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Parameter;
import javax.persistence.Query;
import javax.transaction.Transactional;
import java.math.BigInteger;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@ApplicationScoped
public class PurchaseService implements IAction {

  private static final Logger LOGGER = LoggerFactory.getLogger(PurchaseService.class.getName());

  @Inject
  EntityManager em;

  @Inject
  SupplierService supplierService;

  @Override
  @Transactional
  @TransactionConfiguration(timeout = 30)
  public SimpleResponse insert(Object param, String header) {
    try {

      ObjectMapper om = new ObjectMapper();

      Map<String, Object> reqHeader = om.readValue(header, Map.class);

      if (reqHeader.get("userId") == null || reqHeader.get("userId").toString().equals(GeneralConstants.EMPTY_STRING)){
        return new SimpleResponse(GeneralConstants.VALIDATION_CODE, GeneralConstants.UNAUTHORIZED, new HashMap<>());
      }

      PurchaseModel purchase = om.convertValue(param, PurchaseModel.class);

      if (purchase.purchaseNumber == null || purchase.purchaseNumber.equals(GeneralConstants.EMPTY_STRING)){
        return new SimpleResponse(GeneralConstants.VALIDATION_CODE, "purchase number is required", new HashMap<>());
      }

      if (purchase.date == null){
        return new SimpleResponse(GeneralConstants.VALIDATION_CODE, "date is required", new HashMap<>());
      }

      Map<String, Object> reqParam = om.convertValue(param, Map.class);

      purchase.date = convertFromStringToDateYYYYMMDD(reqParam.get("date").toString());

      ObjectActiveAndCreatedDateUtil.registerObject(purchase, reqHeader.get("userId").toString());
      purchase.persist();

      return new SimpleResponse(GeneralConstants.SUCCESS_CODE, GeneralConstants.SUCCESS, getPurchase(purchase.id));

    }catch (Exception event){
      LOGGER.error(event.getMessage(), event);
      return new SimpleResponse(GeneralConstants.FAIL_CODE, GeneralConstants.FAILED, event.getMessage() + event);
    }
  }

  @Override
  @Transactional
  @TransactionConfiguration(timeout = 30)
  public SimpleResponse update(Object param, String header) {
    try {

      ObjectMapper om = new ObjectMapper();

      Map<String, Object> reqHeader = om.readValue(header, Map.class);

      if (reqHeader.get("userId") == null || reqHeader.get("userId").toString().equals(GeneralConstants.EMPTY_STRING)){
        return new SimpleResponse(GeneralConstants.VALIDATION_CODE, GeneralConstants.UNAUTHORIZED, new HashMap<>());
      }

      Map<String, Object> reqParam = om.convertValue(param, Map.class);

      if (reqParam.get("id") == null || reqParam.get("id").toString().equals(GeneralConstants.EMPTY_STRING)){
        return new SimpleResponse(GeneralConstants.VALIDATION_CODE, "sales id is required", new HashMap<>());
      }

      PurchaseModel purchase = PurchaseModel.findById(reqParam.get("id").toString());

      if (purchase == null){
        return new SimpleResponse(GeneralConstants.VALIDATION_CODE, "sales does not exists", new HashMap<>());
      }

      if (purchase.purchaseNumber != null){
        purchase.purchaseNumber = reqParam.get("purchaseNumber").toString();
      }

      if (purchase.date != null){
        purchase.date = convertFromStringToDateYYYYMMDD(reqParam.get("date").toString());
      }


      ObjectActiveAndCreatedDateUtil.updateObject(purchase, reqHeader.get("userId").toString());
      purchase.persist();

      return new SimpleResponse(GeneralConstants.SUCCESS_CODE, GeneralConstants.SUCCESS, getPurchase(purchase.id));

    }catch (Exception event){
      LOGGER.error(event.getMessage(), event);
      return new SimpleResponse(GeneralConstants.FAIL_CODE, GeneralConstants.FAILED, event + event.getMessage());
    }
  }

  @Override
  public SimpleResponse inquiry(Object param) {
    try {

      ObjectMapper om = new ObjectMapper();

      Map<String, Object> request = om.convertValue(param, new TypeReference<>(){});

      Integer limit = 5, offset = 0;

      Boolean filterPurchaseNumber = false, filterDate = false;

      if (request.containsKey("limit") && request.get("limit") != null){
        limit = Integer.parseInt(request.get("limit").toString());
      }

      if (request.containsKey("offset") && request.get("offset") != null){
        offset = Integer.parseInt(request.get("offset").toString());
      }

      StringBuilder queryString = new StringBuilder();
      queryString.append("select id, purchase_number, date from hary.purchase where true");

      if (request.containsKey("purchaseNumber") && request.get("purchaseNumber") != null){
        queryString.append(" and purchase_number ilike :paramPurchase ");
        filterPurchaseNumber = true;
      }

      if (request.containsKey("date") && request.get("date") != null){
        queryString.append(" and \"date\" ilike :paramDate ");
        filterDate = true;
      }

      queryString.append("order by id desc");

      Query query = em.createNativeQuery(queryString.toString());

      if (filterPurchaseNumber){
        query.setParameter("paramPurchase", "%"+request.get("purchaseNumber").toString()+"%");
      }

      if (filterDate){
        query.setParameter("paramDate", "%"+request.get("date").toString()+"%");
      }

      if (!limit.equals(-99) || !offset.equals(-99) || limit >= 0 || offset >=0){
        query.setMaxResults(limit);
        query.setFirstResult(offset);
      }

      List<Object[]> result = query.getResultList();

      List<Map<String, Object>> data = BasicUtils.createListOfMapFromArray(result, "id", "purchase_number", "date");

      Query qCount = em.createNativeQuery(String.format(queryString.toString().replaceFirst("select.* from", "select count (*) from").replaceFirst("order by.*", "")));

      for (Parameter queryParam : query.getParameters()){
        qCount.setParameter(queryParam.getName(), query.getParameterValue(queryParam));
      }

      BigInteger count = (BigInteger) qCount.getSingleResult();

      Map<String, Object> response = new HashMap<>();
      response.put("data", data);
      response.put("total", count);
      response.put("filtered", data.size());

      return new SimpleResponse(GeneralConstants.SUCCESS_CODE, GeneralConstants.SUCCESS, response);

    }catch (Exception event){
      LOGGER.error(event.getMessage(), event);
      return new SimpleResponse(GeneralConstants.FAIL_CODE, GeneralConstants.FAILED, new HashMap<
          >());
    }
  }

  @Override
  public SimpleResponse entity(Object param) {
    try {

      ObjectMapper om = new ObjectMapper();

      Map<String, Object> reqParam = om.convertValue(param, new TypeReference<>(){});

      if (reqParam.get("id") == null || reqParam.get("id").toString().equals(GeneralConstants.EMPTY_STRING)){
        return new SimpleResponse(GeneralConstants.VALIDATION_CODE, "id is required", new HashMap<>());
      }

      Map<String, Object> response = getPurchase(reqParam.get("id").toString());


      return new SimpleResponse(GeneralConstants.SUCCESS_CODE, GeneralConstants.SUCCESS, response);

    }catch (Exception event){
      LOGGER.error(event.getMessage(), event);
      return new SimpleResponse(GeneralConstants.FAIL_CODE, event.getMessage(), event);
    }
  }

  @Transactional
  @TransactionConfiguration(timeout = 20)
  public SimpleResponse postInsert(Object param, String header){
    try {

      ObjectMapper om = new ObjectMapper();

      Map<String, Object> reqHeader = om.readValue(header, Map.class);

      if (reqHeader.get("userId") == null || reqHeader.get("userId").toString().equals(GeneralConstants.EMPTY_STRING)){
        return new SimpleResponse(GeneralConstants.VALIDATION_CODE,  GeneralConstants.UNAUTHORIZED, new String());
      }

      PurchaseModel purchase = om.convertValue(param, PurchaseModel.class);

      if (purchase.purchaseNumber == null || purchase.purchaseNumber.equals(GeneralConstants.EMPTY_STRING)){
        return new SimpleResponse(GeneralConstants.VALIDATION_CODE, "purchase number is required", new String());
      }

      if (purchase.date == null){
        return new SimpleResponse(GeneralConstants.VALIDATION_CODE, "date is required", new String());
      }

      if (purchase.supplier == null){
        return new SimpleResponse(GeneralConstants.VALIDATION_CODE, "supplier is required", new String());
      }


      Query result = insertSales(purchase.purchaseNumber, purchase.date, purchase.supplier, reqHeader.get("userId").toString(), reqHeader.get("userId").toString());

      result.executeUpdate();

      Object[] response = findBy(purchase.purchaseNumber, purchase.date, purchase.supplier, reqHeader.get("userId").toString(), reqHeader.get("userId").toString());

      return new SimpleResponse(GeneralConstants.SUCCESS_CODE, GeneralConstants.SUCCESS, getPurchase(response[0].toString()));

    }catch (Exception event){
      LOGGER.error(event.getMessage(), event);
      return new SimpleResponse(GeneralConstants.FAIL_CODE, event.getMessage(), event);
    }
  }

  @Transactional
  @TransactionConfiguration(timeout = 20)
  public SimpleResponse postUpdate(Object param, String header){
    try {

      ObjectMapper om = new ObjectMapper();

      Map<String, Object> reqHeader = om.readValue(header, Map.class);

      if (reqHeader.get("userId") == null || reqHeader.get("userId").toString().equals(GeneralConstants.EMPTY_STRING)){
        return new SimpleResponse(GeneralConstants.VALIDATION_CODE,  GeneralConstants.UNAUTHORIZED, new String());
      }

      Map<String, Object> reqParam = om.convertValue(param, Map.class);

      if (reqParam.get("id") == null || reqParam.get("id").toString().equals(GeneralConstants.EMPTY_STRING))
        return new SimpleResponse(GeneralConstants.VALIDATION_CODE, "id is required", new String());

      PurchaseModel purchase = PurchaseModel.findById(reqParam.get("id").toString());

      if (purchase == null)
        return new SimpleResponse(GeneralConstants.VALIDATION_CODE, "purchase does not exist", new String());

      Query result = updatePurchase(reqParam.get("id").toString(), reqParam.get("purchaseNumber").toString(), reqParam.get("date").toString(), reqParam.get("supplier"), reqHeader.get("userId").toString());

      result.executeUpdate();

      return new SimpleResponse(GeneralConstants.SUCCESS_CODE, GeneralConstants.SUCCESS, getPurchase(reqParam.get("id").toString()));

    }catch (Exception event){
      LOGGER.error(event.getMessage(), event);
      return new SimpleResponse(GeneralConstants.FAIL_CODE, event.getMessage(), event);
    }
  }

  @Transactional
  public SimpleResponse postDelete(Object param, String header){
    try {

      ObjectMapper om = new ObjectMapper();

      Map<String, Object> reqHeader = om.readValue(header, Map.class);

      if (reqHeader.get("userId") == null || reqHeader.get("userId").toString().equals(GeneralConstants.EMPTY_STRING))
        return new SimpleResponse(GeneralConstants.VALIDATION_CODE,  GeneralConstants.UNAUTHORIZED, new String());


      Map<String, Object> reqParam = om.convertValue(param, Map.class);

      if (reqParam.get("id") == null || reqParam.get("id").toString().equals(GeneralConstants.EMPTY_STRING))
        return new SimpleResponse(GeneralConstants.VALIDATION_CODE, "id is required", new String());

      PurchaseModel sales = PurchaseModel.findById(reqParam.get("id").toString());

      if (sales == null){
        return new SimpleResponse(GeneralConstants.VALIDATION_CODE, "purchase does not exists", GeneralConstants.EMPTY_STRING);
      }

      deletePurchase(reqParam.get("id").toString()).executeUpdate();

      return new SimpleResponse(GeneralConstants.SUCCESS_CODE, "purchase has been deleted", GeneralConstants.EMPTY_STRING);

    }catch (Exception event){
      LOGGER.error(event.getMessage(), event);
      return new SimpleResponse(GeneralConstants.FAIL_CODE, event.getMessage(), event);
    }
  }

  public Object[] findBy(String purchaseNumber, Date date, Object supplier, String createdBy, String updateBy){
    Query findQuery = em.createNativeQuery("select id, purchase_number, date, supplier_id from hary.purchase where purchase_number = :purchaseNumber and date = :date and supplier_id = :supplier and created_by = :createdBy and updated_by = :updatedBy");
    findQuery.setParameter("purchaseNumber", purchaseNumber);
    findQuery.setParameter("date", date);

    ObjectMapper om = new ObjectMapper();

    findQuery.setParameter("supplier", om.convertValue(supplier, SupplierModel.class));
    findQuery.setParameter("createdBy", createdBy);
    findQuery.setParameter("updatedBy", updateBy);

    return (Object[]) findQuery.getSingleResult();
  }

  public Query insertSales(String purchaseNumber, Date date, Object supplier, String createdBy, String updateBy) throws ParseException {
    StringBuilder sb = new StringBuilder();
    sb.append("insert into hary.purchase");
    sb.append(" (id, created_at, created_by, updated_at, updated_by, \"version\", \"date\", purchase_number, supplier_id) values");
    sb.append(" (gen_random_uuid(), pg_catalog.now(), :createdBy, pg_catalog.now(), :updatedBy, 0, :date, :purchaseNumber, :supplier)");

    Query query = em.createNativeQuery(sb.toString());
    query.setParameter("purchaseNumber", purchaseNumber);
    query.setParameter("date", date);

    query.setParameter("supplier", supplier);
    query.setParameter("createdBy", createdBy);
    query.setParameter("updatedBy", updateBy);

    return query;
  }

  public Query updatePurchase(String id, String purchaseNumber, String date, Object supplier, String updatedBy) throws ParseException {
    StringBuilder sb = new StringBuilder();
    sb.append("UPDATE hary.purchase");
    sb.append(" SET updated_at = pg_catalog.now(), updated_by = :updatedBy, \"version\" = :version, purchase_number = :purchaseNumber, \"date\" = :date, supplier_id = :supplier");
    sb.append(" where id = :id");

    Query query = em.createNativeQuery(sb.toString());

    if (id != null)
      query.setParameter("id", id);

    if (purchaseNumber != null)
      query.setParameter("purchaseNumber", purchaseNumber);

    if (date != null)
      query.setParameter("date", convertFromStringToDateYYYYMMDD(date));

    ObjectMapper om = new ObjectMapper();

    if (supplier != null)
      query.setParameter("supplier", om.convertValue(supplier, CustomerModel.class));

    if (updatedBy != null)
      query.setParameter("updatedBy", updatedBy);

    Object[] versionPurchase = getVersionPurchase(id);

    query.setParameter("version", Long.parseLong(versionPurchase[4].toString()) + 1L);

    return query;
  }

  public Query deletePurchase(String id){
    Query query = em.createNativeQuery("delete from hary.purchase where id = :id");

    if (id != null)
      query.setParameter("id", id);

    return query;
  }

  public Object[] getVersionPurchase(String id){

    StringBuilder sb = new StringBuilder();
    sb.append("select id, purchase_number, date, supplier_id, version from hary.purchase where id = :id");

    Query query = em.createNativeQuery(sb.toString());
    query.setParameter("id", id);

    return (Object[]) query.getSingleResult();

  }

  public Map<String, Object> getPurchase(String id) throws Exception {

    StringBuilder sb = new StringBuilder();
    sb.append("select id, purchase_number, date, supplier_id ");
    sb.append(" from hary.purchase");
    sb.append(" where id = :id");

    Query query = em.createNativeQuery(sb.toString());
    query.setParameter("id", id);

    Object[] result = (Object[]) query.getSingleResult();

    Map<String, Object> data = BasicUtils.createMapFromArray(result, "id", "purchase_number", "date", "supplier");

    Map<String, Object> response = new HashMap<>();


    Map<String, Object> supplier = supplierService.getSupplier(result[3].toString());

    data.replace("supplier", supplier);

    return data;
  }

  private Date convertFromStringToDateYYYYMMDD(String date) throws ParseException {
    return new SimpleDateFormat(DateUtil.DATE_PATTERN).parse(date);
  }
}
