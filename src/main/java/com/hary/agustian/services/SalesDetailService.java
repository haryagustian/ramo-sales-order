package com.hary.agustian.services;

import com.barrans.util.*;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.hary.agustian.models.ProductModel;
import com.hary.agustian.models.SalesDetailModel;
import com.hary.agustian.models.SalesModel;
import io.quarkus.narayana.jta.runtime.TransactionConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Parameter;
import javax.persistence.Query;
import javax.transaction.Transactional;
import java.math.BigInteger;
import java.text.ParseException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@ApplicationScoped
public class SalesDetailService implements IAction {

  private static final Logger LOGGER = LoggerFactory.getLogger(SalesDetailService.class.getName());

  @Inject
  EntityManager em;

  @Inject
  SalesService salesService;

  @Inject
  ProductService productService;

  @Override
  @Transactional
  @TransactionConfiguration(timeout = 30)
  public SimpleResponse insert(Object param, String header) {
    try {

      ObjectMapper om = new ObjectMapper();

      Map<String, Object> reqHeader = om.readValue(header, Map.class);

      if (reqHeader.get("userId") == null || reqHeader.get("userId").toString().equals(GeneralConstants.EMPTY_STRING)){
        return new SimpleResponse(GeneralConstants.VALIDATION_CODE, GeneralConstants.UNAUTHORIZED, new HashMap<>());
      }

      SalesDetailModel sd = om.convertValue(param, SalesDetailModel.class);

      if (sd.quantity == null){
        return new SimpleResponse(GeneralConstants.VALIDATION_CODE, "quantity is required", new HashMap<>());
      }

      if (sd.sales == null || sd.sales.toString().equals(GeneralConstants.EMPTY_STRING)){
        return new SimpleResponse(GeneralConstants.VALIDATION_CODE, "sales is required", new HashMap<>());
      }

      if (sd.product == null || sd.product.toString().equals(GeneralConstants.EMPTY_STRING)){
        return new SimpleResponse(GeneralConstants.VALIDATION_CODE, "product is required", new HashMap<>());
      }

      ObjectActiveAndCreatedDateUtil.registerObject(sd, reqHeader.get("userId").toString());
      sd.persist();

      return new SimpleResponse(GeneralConstants.SUCCESS_CODE, GeneralConstants.SUCCESS, getSalesDetail(sd.id));

    }catch (Exception event){
      LOGGER.error(event.getMessage(), event);
      return new SimpleResponse(GeneralConstants.FAIL_CODE, GeneralConstants.FAILED, event.getMessage() + event);
    }
  }

  @Override
  @Transactional
  @TransactionConfiguration(timeout = 30)
  public SimpleResponse update(Object param, String header) {
    try {

      ObjectMapper om = new ObjectMapper();

      Map<String, Object> reqHeader = om.readValue(header, Map.class);

      if (reqHeader.get("userId") == null || reqHeader.get("userId").toString().equals(GeneralConstants.EMPTY_STRING)){
        return new SimpleResponse(GeneralConstants.VALIDATION_CODE, GeneralConstants.UNAUTHORIZED, new HashMap<>());
      }

      Map<String, Object> reqParam = om.convertValue(param, Map.class);

      if (reqParam.get("id") == null || reqParam.get("id").toString().equals(GeneralConstants.EMPTY_STRING)){
        return new SimpleResponse(GeneralConstants.VALIDATION_CODE, GeneralConstants.UNAUTHORIZED, new HashMap<>());
      }

      SalesDetailModel sd = SalesDetailModel.findById(reqParam.get("id").toString());

      if (sd == null){
        return new SimpleResponse(GeneralConstants.VALIDATION_CODE, "sales detail does not exists", new HashMap<>());
      }

      if (sd.quantity != null){
        sd.quantity = Integer.parseInt(reqParam.get("quantity").toString());
      }

      if (sd.sales != null){
        sd.sales = om.convertValue(reqParam.get("sales"), SalesModel.class);
      }

      if (sd.product != null){
        sd.product = om.convertValue(reqParam.get("product"), ProductModel.class);
      }

      ObjectActiveAndCreatedDateUtil.updateObject(sd, reqHeader.get("userId").toString());
      sd.persist();

      return new SimpleResponse(GeneralConstants.SUCCESS_CODE, GeneralConstants.SUCCESS, getSalesDetail(sd.id));

    }catch (Exception event){
      LOGGER.error(event.getMessage(), event);
      return new SimpleResponse(GeneralConstants.FAIL_CODE, GeneralConstants.FAILED, event + event.getMessage());
    }
  }

  @Override
  public SimpleResponse inquiry(Object param) {
    try {

      ObjectMapper om = new ObjectMapper();

      Map<String, Object> request = om.convertValue(param, new TypeReference<>(){});

      Integer limit = 5, offset = 0;

      Boolean filterQuantity = false;

      if (request.containsKey("limit") && request.get("limit") != null){
        limit = Integer.parseInt(request.get("limit").toString());
      }

      if (request.containsKey("offset") && request.get("offset") != null){
        offset = Integer.parseInt(request.get("offset").toString());
      }

      StringBuilder queryString = new StringBuilder();
      queryString.append("select id, quantity from hary.sales_detail where true");

      if (request.containsKey("quantity") && request.get("quantity") != null){
        queryString.append(" and quantity = :paramQuantity ");
        filterQuantity = true;
      }

      queryString.append("order by id desc");

      Query query = em.createNativeQuery(queryString.toString());

      if (filterQuantity){
        query.setParameter("paramQuantity", Integer.parseInt(request.get("quantity").toString()));
      }

      if (!limit.equals(-99) || !offset.equals(-99) || limit >= 0 || offset >=0){
        query.setMaxResults(limit);
        query.setFirstResult(offset);
      }

      List<Object[]> result = query.getResultList();

      List<Map<String, Object>> data = BasicUtils.createListOfMapFromArray(result, "id", "quantity");

      Query qCount = em.createNativeQuery(String.format(queryString.toString().replaceFirst("select.* from", "select count (*) from").replaceFirst("order by.*", "")));

      for (Parameter queryParam : query.getParameters()){
        qCount.setParameter(queryParam.getName(), query.getParameterValue(queryParam));
      }

      BigInteger count = (BigInteger) qCount.getSingleResult();

      Map<String, Object> response = new HashMap<>();
      response.put("data", data);
      response.put("total", count);
      response.put("filtered", data.size());

      if (data.size() == 0){
        response.put("message", "sales detail there's not yet");
      }

      return new SimpleResponse(GeneralConstants.SUCCESS_CODE, GeneralConstants.SUCCESS, response);

    }catch (Exception event){
      LOGGER.error(event.getMessage(), event);
      return new SimpleResponse(GeneralConstants.FAIL_CODE, GeneralConstants.FAILED, new HashMap<
          >());
    }
  }

  @Override
  public SimpleResponse entity(Object param) {
    try {

      ObjectMapper om = new ObjectMapper();

      Map<String, Object> reqParam = om.convertValue(param, new TypeReference<>(){});

      if (reqParam.get("id") == null || reqParam.get("id").toString().equals(GeneralConstants.EMPTY_STRING)){
        return new SimpleResponse(GeneralConstants.VALIDATION_CODE, GeneralConstants.UNAUTHORIZED, new HashMap<>());
      }

      SalesDetailModel response = SalesDetailModel.findById(reqParam.get("id").toString());

      if (response == null){return new SimpleResponse(GeneralConstants.VALIDATION_CODE, "sales detail does not exists", new HashMap<>());
      }

      return new SimpleResponse(GeneralConstants.SUCCESS_CODE, GeneralConstants.SUCCESS, response);

    }catch (Exception event){
      LOGGER.error(event.getMessage(), event);
      return new SimpleResponse(GeneralConstants.FAIL_CODE, event.getMessage(), event);
    }
  }

  @Transactional
  @TransactionConfiguration(timeout = 20)
  public SimpleResponse postInsert(Object param, String header){
    try {

      ObjectMapper om = new ObjectMapper();

      Map<String, Object> reqHeader = om.readValue(header, Map.class);

      if (reqHeader.get("userId") == null || reqHeader.get("userId").toString().equals(GeneralConstants.EMPTY_STRING)){
        return new SimpleResponse(GeneralConstants.VALIDATION_CODE,  GeneralConstants.UNAUTHORIZED, new String());
      }

      SalesDetailModel sd = om.convertValue(param, SalesDetailModel.class);

      if (sd.quantity == null){
        return new SimpleResponse(GeneralConstants.VALIDATION_CODE, "quantity is required", new String());
      }

      if (sd.sales == null){
        return new SimpleResponse(GeneralConstants.VALIDATION_CODE, "sales is required", new String());
      }

      if (sd.product == null){
        return new SimpleResponse(GeneralConstants.VALIDATION_CODE, "product is required", new String());
      }


      Query result = insertSalesDetail(sd.quantity, sd.sales, sd.product, reqHeader.get("userId").toString(), reqHeader.get("userId").toString());

      result.executeUpdate();

      Object[] response = findBy(sd.quantity, sd.sales, sd.product, reqHeader.get("userId").toString(), reqHeader.get("userId").toString());

      return new SimpleResponse(GeneralConstants.SUCCESS_CODE, GeneralConstants.SUCCESS, getSalesDetail(response[0].toString()));

    }catch (Exception event){
      LOGGER.error(event.getMessage(), event);
      return new SimpleResponse(GeneralConstants.FAIL_CODE, event.getMessage(), event);
    }
  }

  @Transactional
  @TransactionConfiguration(timeout = 20)
  public SimpleResponse postUpdate(Object param, String header){
    try {

      ObjectMapper om = new ObjectMapper();

      Map<String, Object> reqHeader = om.readValue(header, Map.class);

      if (reqHeader.get("userId") == null || reqHeader.get("userId").toString().equals(GeneralConstants.EMPTY_STRING)){
        return new SimpleResponse(GeneralConstants.VALIDATION_CODE,  GeneralConstants.UNAUTHORIZED, new String());
      }

      Map<String, Object> reqParam = om.convertValue(param, Map.class);

      if (reqParam.get("id") == null || reqParam.get("id").toString().equals(GeneralConstants.EMPTY_STRING))
        return new SimpleResponse(GeneralConstants.VALIDATION_CODE, "id is required", new String());

      SalesDetailModel sd = SalesDetailModel.findById(reqParam.get("id").toString());

      if (sd == null)
        return new SimpleResponse(GeneralConstants.VALIDATION_CODE, "sales detail does not exist", new String());

      Query result = updateSalesDetail(reqParam.get("id").toString(), Integer.parseInt(reqParam.get("quantity").toString()), reqParam.get("sales"), reqParam.get("product"), reqHeader.get("userId").toString());

      result.executeUpdate();

      return new SimpleResponse(GeneralConstants.SUCCESS_CODE, GeneralConstants.SUCCESS, getSalesDetail(reqParam.get("id").toString()));

    }catch (Exception event){
      LOGGER.error(event.getMessage(), event);
      return new SimpleResponse(GeneralConstants.FAIL_CODE, event.getMessage(), event);
    }
  }

  @Transactional
  public SimpleResponse postDelete(Object param, String header){
    try {

      ObjectMapper om = new ObjectMapper();

      Map<String, Object> reqHeader = om.readValue(header, Map.class);

      if (reqHeader.get("userId") == null || reqHeader.get("userId").toString().equals(GeneralConstants.EMPTY_STRING))
        return new SimpleResponse(GeneralConstants.VALIDATION_CODE,  GeneralConstants.UNAUTHORIZED, new String());


      Map<String, Object> reqParam = om.convertValue(param, Map.class);

      if (reqParam.get("id") == null || reqParam.get("id").toString().equals(GeneralConstants.EMPTY_STRING))
        return new SimpleResponse(GeneralConstants.VALIDATION_CODE, "id is required", new String());

      SalesDetailModel sd = SalesDetailModel.findById(reqParam.get("id").toString());

      if (sd == null){
        return new SimpleResponse(GeneralConstants.VALIDATION_CODE, "sales detail does not exists", GeneralConstants.EMPTY_STRING);
      }

      deleteSalesDetail(reqParam.get("id").toString()).executeUpdate();

      return new SimpleResponse(GeneralConstants.SUCCESS_CODE, "sales detail has been deleted", GeneralConstants.EMPTY_STRING);

    }catch (Exception event){
      LOGGER.error(event.getMessage(), event);
      return new SimpleResponse(GeneralConstants.FAIL_CODE, event.getMessage(), event);
    }
  }

  public SimpleResponse getListSalesDetail(){
    try {

      StringBuilder sb = new StringBuilder();
      sb.append("select sd.id, sd.quantity, sd.sales_id, sd.product_id");
      sb.append(" from hary.sales_detail sd order by sd.id desc");

      Query query = em.createNativeQuery(sb.toString());

      List<Object[]> result = query.getResultList();

      List<Map<String, Object>> data = BasicUtils.createListOfMapFromArray(result, "id", "quantity", "sales", "product");

      for (Map<String, Object> sd : data){
        sd.replace("sales", salesService.getSales(sd.get("sales").toString()));
        sd.replace("product", productService.getProduct(sd.get("product").toString()));
      }

      return new SimpleResponse(GeneralConstants.SUCCESS_CODE, GeneralConstants.SUCCESS, data);

    }catch (Exception event){
      LOGGER.error(event.getMessage(), event);
      return new SimpleResponse(GeneralConstants.FAIL_CODE, event.getMessage(), event);
    }
  }

  public SimpleResponse getTransactionList(){
    try {

      StringBuilder sb = new StringBuilder();

      sb.append("select c.name AS \"Customer Name\", c.phone AS \"Customer Phone\", s.date AS \"Date\", s.transaction_number AS \"Transaction Number\", p.\"name\"AS \"Product Name\", sd.quantity AS \"Quantity\", p.price AS \"Price\", SUM(p.price * sd.quantity) as \"Sub Total\" ");
      sb.append(" from hary.customer c");
      sb.append(" inner join hary.sales s on s.customer_id = c.id");
      sb.append(" inner join hary.sales_detail sd on sd.sales_id = s.id");
      sb.append(" inner join hary.product p on p.id = sd.product_id");
      sb.append(" group by c.name, c.phone, s.date, s.transaction_number, p.name, sd.quantity, p.price");

      Query query = em.createNativeQuery(sb.toString());

      List<Map<String, Object>> response = BasicUtils.createListOfMapFromArray(query.getResultList(), "customer_name", "customer_phone", "date", "transaction_number", "product_name", "quantity", "price", "sub_total");

      return new SimpleResponse(GeneralConstants.SUCCESS_CODE, GeneralConstants.SUCCESS, response);

    }catch (Exception event){
      LOGGER.error(event.getMessage(), event);
      return new SimpleResponse(GeneralConstants.FAIL_CODE, GeneralConstants.FAILED, event.getMessage() + event);
    }
  }

  public SimpleResponse getTotalSalesProduct(){
    try {

      StringBuilder sb = new StringBuilder();

      sb.append("select p.\"name\" as \"Product Name\", sum(sd.quantity) as \"Total Quantity\", count (p.\"name\") as \"Total\" ");
      sb.append(" from hary.product p");
      sb.append(" inner join hary.sales_detail sd on sd.product_id = p.id");
      sb.append(" inner join hary.sales s on s.id = sd.sales_id");
      sb.append(" where s.\"date\" between '2022-1-1' and '2022-12-12'");
      sb.append(" group by p.\"name\"");

      Query query = em.createNativeQuery(sb.toString());

      List<Map<String, Object>> response = BasicUtils.createListOfMapFromArray(query.getResultList(), "product_name", "total_quantity", "total");

      return new SimpleResponse(GeneralConstants.SUCCESS_CODE, GeneralConstants.SUCCESS, response);

    }catch (Exception event){
      LOGGER.error(event.getMessage(), event);
      return new SimpleResponse(GeneralConstants.FAIL_CODE, GeneralConstants.FAILED, event.getMessage() + event);
    }
  }


  public SimpleResponse getTotalSalesCustomer(){
    try {

      StringBuilder sb = new StringBuilder();

      sb.append(" select c.\"name\" as \"Customer Name\", sum(sd.quantity) as \"Total\"");
      sb.append(" from hary.customer c");
      sb.append(" inner join hary.sales s on s.customer_id  = c.id");
      sb.append(" inner join hary.sales_detail sd on sd.sales_id  = s.id");
      sb.append(" where s.\"date\" between '2022-1-1' and '2022-12-12'");
      sb.append(" group by c.\"name\"");

      Query query = em.createNativeQuery(sb.toString());

      List<Map<String, Object>> response = BasicUtils.createListOfMapFromArray(query.getResultList(), "customer_name", "total");

      return new SimpleResponse(GeneralConstants.SUCCESS_CODE, GeneralConstants.SUCCESS, response);

    }catch (Exception event){
      LOGGER.error(event.getMessage(), event);
      return new SimpleResponse(GeneralConstants.FAIL_CODE, GeneralConstants.FAILED, event.getMessage() + event);
    }
  }

  public Object[] findBy(Integer quantity, Object sales, Object product, String createdBy, String updateBy){
    Query findQuery = em.createNativeQuery("select id, quantity, sales_id, product_id from hary.sales_detail where quantity = :quantity and sales_id = :sales and product_id = :product and created_by = :createdBy and updated_by = :updatedBy");

    findQuery.setParameter("quantity", quantity);

    ObjectMapper om = new ObjectMapper();

    findQuery.setParameter("sales", om.convertValue(sales, SalesModel.class));
    findQuery.setParameter("product", om.convertValue(product, ProductModel.class));
    findQuery.setParameter("createdBy", createdBy);
    findQuery.setParameter("updatedBy", updateBy);

    return (Object[]) findQuery.getSingleResult();
  }

  public Query insertSalesDetail(Integer quantity, Object sales, Object product, String createdBy, String updateBy) throws ParseException {
    StringBuilder sb = new StringBuilder();
    sb.append("insert into hary.sales_detail");
    sb.append(" (id, created_at, created_by, updated_at, updated_by, \"version\", quantity, product_id, sales_id) values");
    sb.append(" (gen_random_uuid(), pg_catalog.now(), :createdBy, pg_catalog.now(), :updatedBy, 0, :quantity, :product, :sales)");

    Query query = em.createNativeQuery(sb.toString());
    query.setParameter("quantity", quantity);
    query.setParameter("sales", sales);

    query.setParameter("product", product);
    query.setParameter("createdBy", createdBy);
    query.setParameter("updatedBy", updateBy);

    return query;
  }

  public Query updateSalesDetail(String id, Integer quantity, Object sales, Object product, String updatedBy) throws ParseException {
    StringBuilder sb = new StringBuilder();
    sb.append("UPDATE hary.sales_detail");
    sb.append(" SET updated_at = pg_catalog.now(), updated_by = :updatedBy, \"version\" = :version, quantity = :quantity, product_id = :product, sales_id = :sales");
    sb.append(" where id = :id");

    Query query = em.createNativeQuery(sb.toString());

    if (id != null)
      query.setParameter("id", id);

    if (quantity != null)
      query.setParameter("quantity", quantity);

    ObjectMapper om = new ObjectMapper();

    if (sales != null)
      query.setParameter("sales", om.convertValue(sales, SalesModel.class));

    if (product != null)
      query.setParameter("product", om.convertValue(product, ProductModel.class));

    if (updatedBy != null)
      query.setParameter("updatedBy", updatedBy);

    Object[] versionSalesDetail = getVersionSalesDetail(id);

    query.setParameter("version", Long.parseLong(versionSalesDetail[4].toString()) + 1L);

    return query;
  }

  public Query deleteSalesDetail(String id){
    Query query = em.createNativeQuery("delete from hary.sales_detail where id = :id");

    if (id != null)
      query.setParameter("id", id);

    return query;
  }

  public Object[] getVersionSalesDetail(String id){

    StringBuilder sb = new StringBuilder();
    sb.append("select id, quantity, sales_id, product_id, version from hary.sales_detail where id = :id");

    Query query = em.createNativeQuery(sb.toString());
    query.setParameter("id", id);

    return (Object[]) query.getSingleResult();

  }

  public Map<String, Object> getSalesDetail(String id) throws Exception {

    StringBuilder sb = new StringBuilder();
    sb.append("select id, quantity, sales_id, product_id");
    sb.append(" from hary.sales_detail");
    sb.append(" where id = :id");

    Query query = em.createNativeQuery(sb.toString());
    query.setParameter("id", id);

    Object[] result = (Object[]) query.getSingleResult();

    Map<String, Object> data = BasicUtils.createMapFromArray(result, "id","quantity", "sales", "product");

    Map<String, Object> sales = salesService.getSales(result[2].toString());
    Map<String, Object> product = productService.getProduct(result[3].toString());

    data.replace("sales", sales);
    data.replace("product", product);

    Map<String, Object> response = new HashMap<>();
    response.put("sales_detail", data);

    return response;
  }


}
