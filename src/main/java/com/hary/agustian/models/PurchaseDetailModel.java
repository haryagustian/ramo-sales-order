package com.hary.agustian.models;

import com.barrans.util.CommonObjectCreatedDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "purchase_detail")
public class PurchaseDetailModel extends CommonObjectCreatedDate implements Serializable {

  private static final Long serialVersionUID = 1L;

  @Column(name = "quantity", nullable = false)
  public Integer quantity;

  @ManyToOne
  public PurchaseModel purchase;

  @ManyToOne
  public ProductModel product;
}
