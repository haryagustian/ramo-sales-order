package com.hary.agustian.models;

import com.barrans.util.CommonObjectCreatedDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "purchase")
public class PurchaseModel extends CommonObjectCreatedDate implements Serializable {

  private static final Long serialVersionUID = 1L;

  @Column(name = "purchase_number", nullable = false, length = 30)
  public String purchaseNumber;

  @Column(name = "date", nullable = false)
  public Date date;

  @ManyToOne
  public SupplierModel supplier;
}
