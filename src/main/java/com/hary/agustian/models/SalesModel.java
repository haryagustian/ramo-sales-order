package com.hary.agustian.models;

import com.barrans.util.CommonObjectCreatedDate;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "sales")
public class SalesModel extends CommonObjectCreatedDate implements Serializable {

  private static final Long serialVersionUID = 1L;

  @Column(name = "transaction_number", length = 50, nullable = false)
  public String transactionNumber;

  @Column(name = "date", nullable = false)
  public Date date;

  @ManyToOne(cascade = CascadeType.MERGE)
  public CustomerModel customer;

}
