package com.hary.agustian.models;

import com.barrans.util.CommonObjectActiveAndCreatedDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "product")
public class ProductModel extends CommonObjectActiveAndCreatedDate implements Serializable {

  private static final Long serialVersionUID = 1L;

  @Column(name = "name", length = 50, nullable = false)
  public String name;

  @Column(name = "description", columnDefinition = "TEXT")
  public String description;

  @Column(name = "price", nullable = false)
  public Integer price;
}
