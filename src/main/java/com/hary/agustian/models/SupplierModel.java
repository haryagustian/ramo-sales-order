package com.hary.agustian.models;

import com.barrans.util.CommonObjectActiveAndCreatedDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "supplier")
public class SupplierModel extends CommonObjectActiveAndCreatedDate implements Serializable {

  private static final Long serialVersionUID = 1L;

  @Column(name = "name", length = 50, nullable = false)
  public String name;

  @Column(name = "address", nullable = false, columnDefinition = "TEXT")
  public String address;

  @Column(name = "phone", length = 15, nullable = false)
  public String phone;

  @Column(name = "mobile", length = 15, nullable = false)
  public String mobile;
}
