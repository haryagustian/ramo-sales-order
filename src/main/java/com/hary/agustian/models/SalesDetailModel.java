package com.hary.agustian.models;

import com.barrans.util.CommonObjectCreatedDate;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "sales_detail")
public class SalesDetailModel extends CommonObjectCreatedDate implements Serializable {

  private static final Long serialVersionUID = 1L;

  @Column(name = "quantity", nullable = false)
  public Integer quantity;

  @ManyToOne
  public SalesModel sales;

  @ManyToOne
  public ProductModel product;
}
