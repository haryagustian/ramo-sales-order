package com.hary.agustian.models;

import com.barrans.util.CommonObjectActiveAndCreatedDate;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "customer")
public class CustomerModel extends CommonObjectActiveAndCreatedDate implements Serializable {

  private static final Long serialVersionUID = 1L;

  @Column(name = "name", length = 50, nullable = false)
  public String name;

  @Column(name = "phone", length = 15, nullable = false)
  public String phone;

  @Column(name = "address", nullable = false, columnDefinition = "TEXT")
  public String address;

}
