package com.hary.agustian.controllers;


import com.barrans.util.IAction;
import com.barrans.util.SimpleResponse;
import com.hary.agustian.services.PurchaseDetailService;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

@Path("/api/v1/sales-order/purchase-detail")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class PurchaseDetailController implements IAction {

  @Inject
  PurchaseDetailService purchaseDetailService;

  @Override
  @POST
  @Path("/insert")
  public SimpleResponse insert(Object param, @HeaderParam("X-Consumer-Custom-ID") String header) {
    return purchaseDetailService.insert(param, header);
  }

  @Override
  @POST
  @Path("/update")
  public SimpleResponse update(Object param, @HeaderParam("X-Consumer-Custom-ID") String header) {
    return purchaseDetailService.update(param, header);
  }

  @Override
  @POST
  @Path("/inquiry")
  public SimpleResponse inquiry(Object param) {
    return purchaseDetailService.inquiry(param);
  }

  @Override
  @POST
  @Path("/entity")
  public SimpleResponse entity(Object param) {
    return purchaseDetailService.entity(param);
  }

  @POST
  @Path("/postInsert")
  public SimpleResponse postInsert(Object param, @HeaderParam("X-Consumer-Custom-ID") String header) {
    return purchaseDetailService.postInsert(param, header);
  }

  @POST
  @Path("/postUpdate")
  public SimpleResponse postUpdate(Object param, @HeaderParam("X-Consumer-Custom-ID") String header) {
    return purchaseDetailService.postUpdate(param, header);
  }

  @POST
  @Path("/postDelete")
  public SimpleResponse postDelete(Object param, @HeaderParam("X-Consumer-Custom-ID") String header) {
    return purchaseDetailService.postDelete(param, header);
  }

  @POST
  @Path("/getListPurchaseDetail")
  public SimpleResponse getListPurchaseDetail(){
    return purchaseDetailService.getListPurchaseDetail();
  }

  @POST
  @Path("/getTransactionList")
  public SimpleResponse getTransactionList(){
    return purchaseDetailService.getTransactionList();
  }

  @POST
  @Path("/getTotalPurchaseProduct")
  public SimpleResponse getTotalPurchaseProduct(){
    return purchaseDetailService.getTotalPurchaseProduct();
  }

  @POST
  @Path("/getTotalPurchaseSupplier")
  public SimpleResponse getTotalPurchaseSupplier(){
    return purchaseDetailService.getTotalPurchaseSupplier();
  }
}
