package com.hary.agustian.controllers;
import com.barrans.util.IAction;
import com.barrans.util.SimpleResponse;
import com.hary.agustian.services.CustomerService;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

@Path("/api/v1/sales-order/customer")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class CustomerController implements IAction {

  @Inject
  CustomerService customerService;
  @Override
  @POST
  @Path("/insert")
  public SimpleResponse insert(Object param, @HeaderParam("X-Consumer-Custom-ID") String header) {
    return customerService.insert(param, header);
  }

  @Override
  @POST
  @Path("/update")
  public SimpleResponse update(Object param, @HeaderParam("X-Consumer-Custom-ID") String header) {
    return customerService.update(param, header);
  }

  @Override
  @POST
  @Path("/inquiry")
  public SimpleResponse inquiry(Object param) {
    return customerService.inquiry(param);
  }

  @Override
  @POST
  @Path("/entity")
  public SimpleResponse entity(Object param) {
    return customerService.entity(param);
  }

  @POST
  @Path("/getListCustomer")
  public SimpleResponse getListCustomer() {
    return customerService.getListCustomer();
  }

  @POST
  @Path("/postInsert")
  public SimpleResponse postInsert(Object param, @HeaderParam("X-Consumer-Custom-ID") String header) {
    return customerService.postInsert(param, header);
  }

  @POST
  @Path("/postUpdate")
  public SimpleResponse postUpdate(Object param, @HeaderParam("X-Consumer-Custom-ID") String header) {
    return customerService.postUpdate(param, header);
  }

  @POST
  @Path("/postDelete")
  public SimpleResponse postDelete(Object param, @HeaderParam("X-Consumer-Custom-ID") String header) {
    return customerService.postDelete(param, header);
  }

}
