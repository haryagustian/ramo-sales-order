package com.hary.agustian.controllers;

import com.barrans.util.IAction;
import com.barrans.util.SimpleResponse;
import com.hary.agustian.services.ProductService;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

@Path("/api/v1/sales-order/product")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class ProductController implements IAction {
  
  @Inject
  ProductService productService;
  
  @Override
  @POST
  @Path("/insert")
  public SimpleResponse insert(Object param, @HeaderParam("X-Consumer-Custom-ID") String header) {
    return productService.insert(param, header);
  }

  @Override
  @POST
  @Path("/update")
  public SimpleResponse update(Object param, @HeaderParam("X-Consumer-Custom-ID") String header) {
    return productService.update(param, header);
  }

  @Override
  @POST
  @Path("/inquiry")
  public SimpleResponse inquiry(Object param) {
    return productService.inquiry(param);
  }

  @Override
  @POST
  @Path("/entity")
  public SimpleResponse entity(Object param) {
    return productService.entity(param);
  }

  @POST
  @Path("/getListProduct")
  public SimpleResponse getListProduct() {
    return productService.getListProduct();
  }

  @POST
  @Path("/postInsert")
  public SimpleResponse postInsert(Object param, @HeaderParam("X-Consumer-Custom-ID") String header) {
    return productService.postInsert(param, header);
  }

  @POST
  @Path("/postUpdate")
  public SimpleResponse postUpdate(Object param, @HeaderParam("X-Consumer-Custom-ID") String header) {
    return productService.postUpdate(param, header);
  }

  @POST
  @Path("/postDelete")
  public SimpleResponse postDelete(Object param, @HeaderParam("X-Consumer-Custom-ID") String header) {
    return productService.postDelete(param, header);
  }
}
