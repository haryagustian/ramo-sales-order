package com.hary.agustian.controllers;

import com.barrans.util.IAction;
import com.barrans.util.SimpleResponse;
import com.hary.agustian.services.SalesService;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

@Path("/api/v1/sales-order/sales")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class SalesController implements IAction {

  @Inject
  SalesService salesService;

  @Override
  @POST
  @Path("/insert")
  public SimpleResponse insert(Object param, @HeaderParam("X-Consumer-Custom-ID") String header) {
    return salesService.insert(param, header);
  }

  @Override
  @POST
  @Path("/update")
  public SimpleResponse update(Object param, @HeaderParam("X-Consumer-Custom-ID") String header) {
    return salesService.update(param, header);
  }

  @Override
  @POST
  @Path("/inquiry")
  public SimpleResponse inquiry(Object param) {
    return salesService.inquiry(param);
  }

  @Override
  @POST
  @Path("/entity")
  public SimpleResponse entity(Object param) {
    return salesService.entity(param);
  }

  @POST
  @Path("/postInsert")
  public SimpleResponse postInsert(Object param, @HeaderParam("X-Consumer-Custom-ID") String header) {
    return salesService.postInsert(param, header);
  }

  @POST
  @Path("/postUpdate")
  public SimpleResponse postUpdate(Object param, @HeaderParam("X-Consumer-Custom-ID") String header) {
    return salesService.postUpdate(param, header);
  }

  @POST
  @Path("/postDelete")
  public SimpleResponse postDelete(Object param, @HeaderParam("X-Consumer-Custom-ID") String header) {
    return salesService.postDelete(param, header);
  }
}
