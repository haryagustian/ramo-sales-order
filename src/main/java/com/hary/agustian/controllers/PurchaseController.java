package com.hary.agustian.controllers;


import com.barrans.util.IAction;
import com.barrans.util.SimpleResponse;
import com.hary.agustian.services.PurchaseService;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

@Path("/api/v1/sales-order/purchase")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class PurchaseController implements IAction {

  @Inject
  PurchaseService purchaseService;

  @Override
  @POST
  @Path("/insert")
  public SimpleResponse insert(Object o, @HeaderParam("X-Consumer-Custom-ID") String s) {
    return purchaseService.insert(o, s);
  }

  @Override
  @POST
  @Path("/update")
  public SimpleResponse update(Object o, @HeaderParam("X-Consumer-Custom-ID") String s) {
    return purchaseService.update(o, s);
  }

  @Override
  @POST
  @Path("/inquiry")
  public SimpleResponse inquiry(Object o) {
    return purchaseService.inquiry(o);
  }

  @Override
  @POST
  @Path("/entity")
  public SimpleResponse entity(Object o) {
    return purchaseService.entity(o);
  }

  @POST
  @Path("/postInsert")
  public SimpleResponse postInsert(Object param, @HeaderParam("X-Consumer-Custom-ID") String header) {
    return purchaseService.postInsert(param, header);
  }

  @POST
  @Path("/postUpdate")
  public SimpleResponse postUpdate(Object param, @HeaderParam("X-Consumer-Custom-ID") String header) {
    return purchaseService.postUpdate(param, header);
  }

  @POST
  @Path("/postDelete")
  public SimpleResponse postDelete(Object param, @HeaderParam("X-Consumer-Custom-ID") String header) {
    return purchaseService.postDelete(param, header);
  }
}
