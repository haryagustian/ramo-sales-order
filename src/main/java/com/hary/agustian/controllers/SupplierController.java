package com.hary.agustian.controllers;

import com.barrans.util.IAction;
import com.barrans.util.SimpleResponse;
import com.hary.agustian.services.SupplierService;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

@Path("/api/v1/sales-order/supplier")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class SupplierController implements IAction {

  @Inject
  SupplierService supplierService;

  @Override
  @POST
  @Path("/insert")
  public SimpleResponse insert(Object o, @HeaderParam("X-Consumer-Custom-ID") String s) {
    return supplierService.insert(o, s);
  }

  @Override
  @POST
  @Path("/update")
  public SimpleResponse update(Object o, @HeaderParam("X-Consumer-Custom-ID") String s) {
    return supplierService.update(o, s);
  }

  @Override
  @POST
  @Path("/inquiry")
  public SimpleResponse inquiry(Object o) {
    return supplierService.inquiry(o);
  }

  @Override
  @POST
  @Path("/entity")
  public SimpleResponse entity(Object o) {
    return supplierService.entity(o);
  }

  @POST
  @Path("/getListSupplier")
  public SimpleResponse getListSupplier() {
    return supplierService.getListSupplier();
  }

  @POST
  @Path("/postInsert")
  public SimpleResponse postInsert(Object param, @HeaderParam("X-Consumer-Custom-ID") String header) {
    return supplierService.postInsert(param, header);
  }

  @POST
  @Path("/postUpdate")
  public SimpleResponse postUpdate(Object param, @HeaderParam("X-Consumer-Custom-ID") String header) {
    return supplierService.postUpdate(param, header);
  }

  @POST
  @Path("/postDelete")
  public SimpleResponse postDelete(Object param, @HeaderParam("X-Consumer-Custom-ID") String header) {
    return supplierService.postDelete(param, header);
  }
}
