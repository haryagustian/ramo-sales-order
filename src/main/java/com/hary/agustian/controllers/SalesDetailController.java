package com.hary.agustian.controllers;

import com.barrans.util.IAction;
import com.barrans.util.SimpleResponse;
import com.hary.agustian.services.SalesDetailService;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

@Path("/api/v1/sales-order/sales-detail")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class SalesDetailController implements IAction {

  @Inject
  SalesDetailService salesDetailService;

  @Override
  @POST
  @Path("/insert")
  public SimpleResponse insert(Object param, @HeaderParam("X-Consumer-Custom-ID") String header) {
    return salesDetailService.insert(param, header);
  }

  @Override
  @POST
  @Path("/update")
  public SimpleResponse update(Object param, @HeaderParam("X-Consumer-Custom-ID") String header) {
    return salesDetailService.update(param, header);
  }

  @Override
  @POST
  @Path("/inquiry")
  public SimpleResponse inquiry(Object param) {
    return salesDetailService.inquiry(param);
  }

  @Override
  @POST
  @Path("/entity")
  public SimpleResponse entity(Object param) {
    return salesDetailService.entity(param);
  }

  @POST
  @Path("/postInsert")
  public SimpleResponse postInsert(Object param, @HeaderParam("X-Consumer-Custom-ID") String header) {
    return salesDetailService.postInsert(param, header);
  }

  @POST
  @Path("/postUpdate")
  public SimpleResponse postUpdate(Object param, @HeaderParam("X-Consumer-Custom-ID") String header) {
    return salesDetailService.postUpdate(param, header);
  }

  @POST
  @Path("/postDelete")
  public SimpleResponse postDelete(Object param, @HeaderParam("X-Consumer-Custom-ID") String header) {
    return salesDetailService.postDelete(param, header);
  }

  @POST
  @Path("/getListSalesDetail")
  public SimpleResponse getListSalesDetail(){
    return salesDetailService.getListSalesDetail();
  }

  @POST
  @Path("/getTransactionList")
  public SimpleResponse getTransactionList(){
    return salesDetailService.getTransactionList();
  }

  @POST
  @Path("/getTotalSalesProduct")
  public SimpleResponse getTotalSalesProduct(){
    return salesDetailService.getTotalSalesProduct();
  }

  @POST
  @Path("/getTotalSalesCustomer")
  public SimpleResponse getTotalSalesCustomer(){
    return salesDetailService.getTotalSalesCustomer();
  }
}
